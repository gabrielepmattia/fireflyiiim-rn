export const FIELD_TYPE_FIELD = 'field';
export const FIELD_TYPE_HEADER = 'header';
export const FIELD_TYPE_TEXT = 'text';
export const FIELD_TYPE_TEXT_AUTOCOMPLETE = 'text-autocomplete';
// export const FIELD_TYPE_TEXT_CHIPS = "text-chips"
export const FIELD_TYPE_NUMBER = 'number';
export const FIELD_TYPE_SELECT = 'select';
export const FIELD_TYPE_DATE = 'date';
export const FIELD_TYPE_BUTTON = 'button';
export const FIELD_TYPE_CAPTION = 'caption';
export const FIELD_TYPE_FIXED_TEXT = 'text-fixed';
export const FIELD_TYPE_PADDER = 'padder';

export interface FieldItemHeader {
  type: string;
  id: string;
  title: string;
}

export interface FieldItemText {
  type: string;
  id: string;
  title: string;
  placeholder: string;
  value: string;
  onValueChanged?: (newText: string) => void;
}

export interface FieldItemFixedText {
  type: string;
  id: string;
  title: string;
  value: string;
}

export interface FieldItemTextAutoComplete {
  type: string;
  id: string;
  title: string;
  placeholder: string;
  value: string;
  suggestions: Array<string>;
  onValueChanged?: (newText: string) => void;
  onSuggestionSelected?: (index: number) => void;
  onSuggestionsBackdropPress?: () => void;
}

/*
export interface FieldItemTextChips {
  type: string;
  id: string;
  title: string;
  placeholder: string;
  value: string;
  onValueChanged?: (newText: string) => void;
}
*/

export interface FieldItemNumber {
  type: string;
  id: string;
  title: string;
  placeholder: string;
  value: number;
  onValueChanged?: (newNumber: number) => void;
}

export interface FieldItemSelect {
  type: string;
  id: string;
  title: string;
  selected: number;
  options: Array<FieldItemSelectValue>; // id, displayValue
  onSelectedValueChanged?: (selectedIndex: number) => void;
}

export interface FieldItemDate {
  type: string;
  id: string;
  title: string;
  value?: Date;
  placeholderNotSet?: string;
  onDateSelected?: (selectedDate: Date | undefined) => void;
}

export interface FieldItemButton {
  type: string;
  id: string;
  title: string;
  description: string;
  onPressed?: () => void;
  accessoryLeft?: () => React.ReactElement;
  showTopDivider?: boolean;
  showBottomDivider?: boolean;
}

export interface FieldItemCaption {
  type: string;
  id: string;
  text: string;
}

export interface FieldItemPadder {
  type: string;
  id: string;
  height: number;
}

/*
 * Utils types
 */

export interface FieldItemSelectValue {
  id: string;
  value: string;
}

export type FieldItem =
  | FieldItemHeader
  | FieldItemText
  | FieldItemNumber
  | FieldItemSelect
  | FieldItemDate
  | FieldItemButton
  | FieldItemCaption
  | FieldItemPadder;
