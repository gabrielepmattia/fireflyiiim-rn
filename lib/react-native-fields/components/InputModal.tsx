import { Button, Card, List, ListItem, Modal, StyleService, Text, useStyleSheet } from "@ui-kitten/components";
import React from "react";
import { Dimensions, TextInput, View } from "react-native";
import { FieldItemSelectValue } from "../types/fields";

export const INPUT_MODAL_TYPE_SELECT = "select"
export const INPUT_MODAL_TYPE_TEXT = "text"


/*
 * Interfaces
 */

export interface Props {
  visible: boolean;
  type: string;
  title: string;
  options?: Array<FieldItemSelectValue>

  onSetButtonPressed?: () => void;
  onDismissButtonPressed?: () => void;
  onSelectedOptionPressed?: () => void;
  onBackdropPressed?: () => void;
  onOptionItemSelected?: (index: number) => void;
}

/*
 * Component
 */

const InputModal = (props: Props) => {

  const styles = useStyleSheet(themedStyles);

  /*
   * Renders
   */

  const renderListItemModalBody = (item) => {
    return (
      <ListItem onPress={() => props.onOptionItemSelected ? props.onOptionItemSelected(item.index) : ""}
                style={{ paddingHorizontal: 0, margin: 0 }}>
        <Text category='p2'>{item.item.value}</Text>
      </ListItem>)
  }

  const renderModalBody = (): React.ReactElement => {
    switch (props.type) {
      case INPUT_MODAL_TYPE_SELECT:
        return (<List data={props.options} renderItem={renderListItemModalBody}/>);
      case INPUT_MODAL_TYPE_TEXT:
        return (
          <>
            <TextInput
              placeholder='Place your Text'
            />
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
              <Button size='small' appearance='ghost'
                      onPress={() => props.onDismissButtonPressed !== undefined ? props.onDismissButtonPressed() : false}>DISMISS</Button>
              <Button size='small' appearance='ghost'
                      onPress={() => props.onSetButtonPressed !== undefined ? props.onSetButtonPressed() : false}>SET</Button>
            </View>
          </>
        );
      default:
        return (<></>);
    }
  }

  /*
   * Component
   */

  return (
    <Modal
      style={{
        padding: 0,
        width: Dimensions.get('screen').width * 0.65,
        maxHeight: Dimensions.get('screen').height * 0.60
      }}
      visible={props.visible}
      backdropStyle={styles.modalBackdrop}
      onBackdropPress={() => props.onBackdropPressed !== undefined ? props.onBackdropPressed() : false}>

      <Card style={{ borderRadius: 6 }}>
        <Text style={{ paddingBottom: 8 }} category="h6">{props.title}</Text>
        {renderModalBody()}
      </Card>

    </Modal>
  )

}

/*
 * Export
 */

export default InputModal;

/*
 * Styles
 */

const themedStyles = StyleService.create({
  modalBackdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
});
