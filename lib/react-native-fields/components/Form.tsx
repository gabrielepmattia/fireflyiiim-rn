import DateTimePicker from '@react-native-community/datetimepicker';
import {
  Divider,
  Icon,
  List,
  ListItem,
  ListItemElement,
  ListItemProps,
  Popover,
  StyleService,
  Text,
  useStyleSheet,
} from '@ui-kitten/components';
import moment from 'moment';
import React, { useState } from 'react';
import { Dimensions, ListRenderItemInfo, Platform, TextInput, View } from 'react-native';
import {
  FIELD_TYPE_BUTTON,
  FIELD_TYPE_CAPTION,
  FIELD_TYPE_DATE,
  FIELD_TYPE_FIXED_TEXT,
  FIELD_TYPE_HEADER,
  FIELD_TYPE_NUMBER,
  FIELD_TYPE_PADDER,
  FIELD_TYPE_SELECT,
  FIELD_TYPE_TEXT,
  FIELD_TYPE_TEXT_AUTOCOMPLETE,
  FieldItem,
  FieldItemButton,
  FieldItemCaption,
  FieldItemDate,
  FieldItemFixedText,
  FieldItemNumber,
  FieldItemPadder,
  FieldItemSelect,
  FieldItemSelectValue,
  FieldItemText,
  FieldItemTextAutoComplete,
} from '../types/fields';
import InputModal, { INPUT_MODAL_TYPE_SELECT } from './InputModal';

const DATETIME_PICKER_MODE_DATE = 'date';

/*
 * Interfaces
 */

export interface Props {
  fieldItems: Array<FieldItem>;
  theme: Record<string, string>;
  customMapping: Record<string, string>;

  onModalShown?: () => void | undefined;
  onModalHidden?: () => void | undefined;
}

/*
 * Component
 */

const Form = (props: Props) => {
  const styles = useStyleSheet(themedStyles);

  const [dateTimePickerVisible, setDateTimePickerVisible] = useState(false);
  const [dateTimePickerValue, setDateTimePickerValue] = useState(new Date());
  const [dateTimePickerMode, setDateTimePickerMode] = useState(DATETIME_PICKER_MODE_DATE);
  const [dateTimePickerCurrentFieldIndex, setDateTimePickerCurrentFieldIndex] = useState(0);

  const [inputModalCurrentFieldIndex, setInputModalCurrentFieldIndex] = useState(0);
  const [inputModalVisible, setInputModalVisible] = useState(false);
  const [inputModalTitle, setInputModalTitle] = React.useState('');
  const [inputModalType, setInputModalType] = React.useState('');
  const [inputModalOptions, setInputModalOptions] = React.useState(new Array<FieldItemSelectValue>());

  /*
   * Renders
   */

  const renderFieldListItem = (item: ListRenderItemInfo<FieldItem>) => {
    let index = item.index;
    let fieldItem = item.item;

    switch (fieldItem.type) {
      case FIELD_TYPE_SELECT:
        let fieldItemSelect = fieldItem as FieldItemSelect;
        return (
          <View key={fieldItem.id}>
            <ListItem
              title={fieldItem.title}
              style={{ paddingRight: 8, height: 42 }}
              accessoryRight={() => (
                <View
                  style={{
                    margin: 0,
                    padding: 0,
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                  }}>
                  <Text category="p2" style={styles.formSelectText}>
                    {fieldItemSelect.options[fieldItemSelect.selected].value}
                  </Text>
                  <Icon
                    style={{ width: 22, height: 22, margin: 0, padding: 0 }}
                    fill={props.theme['color-basic-500']}
                    name="chevron-right"
                  />
                </View>
              )}
              // accessoryLeft={renderIcon}
              onPress={() => showInputModal(true, index, INPUT_MODAL_TYPE_SELECT, fieldItemSelect)}
            />
            <Divider />
          </View>
        );

      case FIELD_TYPE_TEXT:
        let fieldItemText = fieldItem as FieldItemText;
        return (
          <View key={fieldItem.id}>
            <ListItem
              title={fieldItem.title}
              style={{ paddingRight: 16, height: 42 }}
              accessoryRight={() => (
                <TextInput
                  placeholderTextColor={props.theme['color-basic-400']}
                  style={{ ...styles.formTextInput, fontFamily: props.customMapping.strict['text-font-family'] }}
                  placeholder={fieldItemText.placeholder}
                  onChangeText={(text) => (fieldItemText.onValueChanged ? fieldItemText.onValueChanged(text) : '')}
                  value={fieldItemText.value}
                />
              )}
            />
            <Divider />
          </View>
        );

      case FIELD_TYPE_TEXT_AUTOCOMPLETE:
        let fieldItemTextAutocomplete = fieldItem as FieldItemTextAutoComplete;
        let renderTextField = () => (
          <View key={`${fieldItem.id}-text`}>
            <ListItem
              title={fieldItem.title}
              style={{ paddingRight: 16, height: 42 }}
              accessoryRight={() => (
                <TextInput
                  placeholderTextColor={props.theme['color-basic-400']}
                  style={{ ...styles.formTextInput, fontFamily: props.customMapping.strict['text-font-family'] }}
                  placeholder={fieldItemTextAutocomplete.placeholder}
                  onChangeText={(text) =>
                    fieldItemTextAutocomplete.onValueChanged ? fieldItemTextAutocomplete.onValueChanged(text) : null
                  }
                  value={fieldItemTextAutocomplete.value}
                />
              )}
            />
            <Divider />
          </View>
        );
        return (
          <Popover
            key={`${fieldItem.id}`}
            style={{ marginBottom: 0, maxHeight: 84, width: Dimensions.get('screen').width * 0.5 }}
            visible={fieldItemTextAutocomplete.suggestions !== undefined && fieldItemTextAutocomplete.suggestions.length > 0}
            anchor={renderTextField}
            placement="top end"
            onBackdropPress={
              fieldItemTextAutocomplete.onSuggestionsBackdropPress ? fieldItemTextAutocomplete.onSuggestionsBackdropPress : null
            }>
            <List
              style={{ borderWidth: 1, borderRadius: 6, borderColor: '#eaeaea' }}
              data={fieldItemTextAutocomplete.suggestions}
              renderItem={({ item, index }) => (
                <ListItem
                  style={{ borderRadius: 6 }}
                  onPress={() =>
                    fieldItemTextAutocomplete.onSuggestionSelected ? fieldItemTextAutocomplete.onSuggestionSelected(index) : null
                  }
                  accessoryRight={() => <Text category="p2">{item}</Text>}
                />
              )}
            />
          </Popover>
        );

      case FIELD_TYPE_FIXED_TEXT:
        let fieldItemFixed = fieldItem as FieldItemFixedText;
        return (
          <View key={fieldItem.id}>
            <ListItem
              title={fieldItemFixed.title}
              style={{ paddingRight: 8, height: 42 }}
              accessoryRight={() => (
                <View>
                  <Text category="p2" style={styles.formSelectText}>
                    {fieldItemFixed.value}
                  </Text>
                </View>
              )}
              onPress={() => showInputModal(true, index, INPUT_MODAL_TYPE_SELECT, fieldItemFixed)}
            />
            <Divider />
          </View>
        );

      case FIELD_TYPE_NUMBER:
        let fieldItemNumber = fieldItem as FieldItemNumber;
        return (
          <View key={fieldItem.id}>
            <ListItem
              title={fieldItem.title}
              style={{ paddingRight: 16, height: 42 }}
              accessoryRight={() => (
                <TextInput
                  keyboardType="decimal-pad"
                  placeholderTextColor={props.theme['color-basic-400']}
                  style={{ ...styles.formTextInput, fontFamily: props.customMapping.strict['text-font-family'] }}
                  placeholder={fieldItemNumber.placeholder}
                  onChangeText={(text) =>
                    fieldItemNumber.onValueChanged ? fieldItemNumber.onValueChanged(parseFloat(text)) : ''
                  }
                  // value={fieldItemNumber.value === 0.0 ? "" : fieldItemNumber.value.toString()}
                />
              )}
            />
            <Divider />
          </View>
        );

      case FIELD_TYPE_DATE:
        let fieldItemDate = fieldItem as FieldItemDate;
        return (
          <View key={fieldItem.id}>
            <ListItem
              title={fieldItemDate.title}
              style={{ paddingRight: 8, height: 42 }}
              accessoryRight={() => (
                <View
                  style={{
                    margin: 0,
                    padding: 0,
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                  }}>
                  <Text category="p2" style={styles.formSelectText}>
                    {fieldItemDate.value
                      ? moment(fieldItemDate.value).format('L')
                      : fieldItemDate.placeholderNotSet
                      ? fieldItemDate.placeholderNotSet
                      : '(not set)'}
                  </Text>
                  <Icon
                    style={{ width: 22, height: 22, margin: 0, padding: 0 }}
                    fill={props.theme['color-basic-500']}
                    name="chevron-right"
                  />
                </View>
              )}
              // accessoryLeft={renderIcon}
              onPress={() => showDateTimePicker(index, DATETIME_PICKER_MODE_DATE, fieldItemDate)}
            />
            <Divider />
          </View>
        );

      case FIELD_TYPE_HEADER:
        return (
          <View key={fieldItem.id}>
            <Text
              style={{
                fontSize: 11,
                paddingHorizontal: 16,
                paddingTop: 16,
                paddingBottom: 8,
                textTransform: 'uppercase',
                color: props.theme['color-basic-700'],
              }}>
              {fieldItem.title}
            </Text>
            <Divider />
          </View>
        );

      case FIELD_TYPE_BUTTON:
        let fieldItemButton = fieldItem as FieldItemButton;
        return (
          <View key={fieldItem.id}>
            {fieldItemButton.showTopDivider ? fieldItemButton.showTopDivider : false}
            <ListItem
              title={fieldItemButton.title}
              accessoryLeft={fieldItemButton.accessoryLeft ? fieldItemButton.accessoryLeft : null}
              onPress={fieldItemButton.onPressed ? fieldItemButton.onPressed : undefined}
            />
            {fieldItemButton.showBottomDivider ? fieldItemButton.showBottomDivider : <Divider />}
          </View>
        );

      case FIELD_TYPE_CAPTION:
        let fieldItemCaption = fieldItem as FieldItemCaption;
        return (
          <View key={fieldItemCaption.id}>
            <Text
              style={{
                fontSize: 11,
                paddingHorizontal: 16,
                paddingTop: 8,
                paddingBottom: 8,
                color: props.theme['color-basic-700'],
              }}>
              {fieldItemCaption.text}
            </Text>
            <Divider />
          </View>
        );

      case FIELD_TYPE_PADDER:
        let fieldItemPadder = fieldItem as FieldItemPadder;
        return (
          <View key={fieldItemPadder.id}>
            <View style={{ height: fieldItemPadder.height !== undefined ? fieldItemPadder.height : 16 }} />
          </View>
        );
    }
  };

  /*
   * Utils
   */

  const showInputModal = (visible: boolean, fieldIndex: number, type?: string, fieldItem?: FieldItem) => {
    setInputModalCurrentFieldIndex(fieldIndex);
    setInputModalTitle(fieldItem?.title ? fieldItem?.title : '');
    setInputModalType(type ? type : '');
    if (type === INPUT_MODAL_TYPE_SELECT) {
      setInputModalOptions((fieldItem as FieldItemSelect).options);
    }
    setInputModalVisible(visible);
    if (visible) {
      if (props.onModalShown !== undefined) {
        props.onModalShown();
      }
    } else {
      if (props.onModalHidden !== undefined) {
        props.onModalHidden();
      }
    }
  };

  const showDateTimePicker = (fieldIndex: number, mode?: string, fieldItem?: FieldItemDate) => {
    setDateTimePickerValue(fieldItem?.value ? fieldItem.value : new Date());
    setDateTimePickerValue(fieldItem?.value ? fieldItem?.value : new Date());
    setDateTimePickerMode(mode ? mode : DATETIME_PICKER_MODE_DATE);
    setDateTimePickerCurrentFieldIndex(fieldIndex);
    setDateTimePickerVisible(true);
  };

  const doneDateTimePicker = (event: Event, selectedDate: Date | undefined) => {
    setDateTimePickerVisible(Platform.OS === 'ios');
    let fieldItemDate = props.fieldItems[dateTimePickerCurrentFieldIndex] as FieldItemDate;
    fieldItemDate.onDateSelected !== undefined ? fieldItemDate.onDateSelected(selectedDate) : '';
  };

  /*
   * Callbacks
   */

  const onOptionItemSelectedClb = (selectedIndex: number) => {
    let clb = (props.fieldItems[inputModalCurrentFieldIndex] as FieldItemSelect).onSelectedValueChanged;
    if (clb !== undefined) {
      clb(selectedIndex);
    }
    setInputModalVisible(false);
  };

  /*
   * Component
   */

  return (
    <>
      <List data={props.fieldItems} renderItem={renderFieldListItem} />
      {
        // props.fieldItems.map((fieldItem, i) => {return renderFieldListItem(fieldItem, i); })
      }
      <InputModal
        title={inputModalTitle}
        type={inputModalType}
        options={inputModalOptions}
        visible={inputModalVisible}
        onDismissButtonPressed={() => setInputModalVisible(false)}
        onBackdropPressed={() => setInputModalVisible(false)}
        onOptionItemSelected={(selectedIndex: number) => onOptionItemSelectedClb(selectedIndex)}
      />
      {dateTimePickerVisible && (
        <DateTimePicker
          testID="dateTimePicker"
          mode={dateTimePickerMode}
          value={dateTimePickerValue}
          display="default"
          onChange={doneDateTimePicker}
        />
      )}
    </>
  );
};

/*
 * Export
 */

export default Form;

/*
 * Styles
 */

const themedStyles = StyleService.create({
  formTextInput: {
    margin: 0,
    padding: 0,
    fontSize: 14,
    textAlign: 'right',
    fontWeight: '400',
    width: Dimensions.get('screen').width * 0.5,
    color: 'color-basic-600', // same of color-basic-600
  },
  formSelectText: {
    margin: 0,
    padding: 0,
    textAlign: 'right',
    fontSize: 14,
    maxWidth: Dimensions.get('screen').width * 0.5,
    color: 'color-basic-500', // same of color-basic-600
  },
});
