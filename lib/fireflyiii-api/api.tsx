import { AccountAttributes } from 'lib/fireflyiii-api/models/account';
import { ApiResponse } from 'lib/fireflyiii-api/models/api';
import { BudgetAttributes } from 'lib/fireflyiii-api/models/budget';
import { CategoryAttributes } from 'lib/fireflyiii-api/models/categories';
import { ApiChartData } from 'lib/fireflyiii-api/models/charts';
import { DataElement } from 'lib/fireflyiii-api/models/data_elements';
import { Summary } from 'lib/fireflyiii-api/models/summary';
import { TagAttributes } from 'lib/fireflyiii-api/models/tags';
import { TransactionAttributes, TransactionStoreElement } from 'lib/fireflyiii-api/models/transaction';

const MODULE = "FireflyIIIApi"

export class FireflyIIIApi {

  private apiUrl: string = ""
  private token: string = ""

  constructor(apiUrl: string | undefined, token: string | undefined) {
    this.apiUrl = apiUrl !== undefined ? apiUrl : ""
    this.token = token !== undefined ? token : ""
  }

  public setAuthData(apiUrl: string, token: string) {
    this.apiUrl = apiUrl
    this.token = token
  }

  public getApiUrl(): string {
    return this.apiUrl
  }

  public getToken(): string {
    return this.token
  }

  private interceptor(apiName: string) {
    console.debug(`[${MODULE}] called ${apiName} apiUrl=${this.apiUrl.length} token=${this.token.length}`)
  }

  /*
   * Authentication
   */

  public static loginWithJWT = (apiUrl: string, token: string): Promise<Response> => {
    return fetch(`${apiUrl}/api/v1/about`, {
      method: 'GET',
      headers: FireflyIIIApi.generateHeaders(token),
    });
  };

  /*
   * Exported
   */

  public getAccounts(type = ''): Promise<Array<DataElement<AccountAttributes>>> {
    this.interceptor("getAccounts")
    return new Promise<Array<DataElement<AccountAttributes>>>((resolve, reject) => {
      FireflyIIIApi.getAccountsRaw(this.apiUrl, this.token, type)
        .then((res) => res.json())
        .catch((err) => reject(err))
        .then((json) => {
          let response = json as ApiResponse<AccountAttributes>;
          resolve(response.data);
        })
        .catch((err) => reject(err));
    });
  }

  public getTransactionsForAccount(
    accountId = '1',
    type = 'all',
    page = '',
    limit = '',
    start = '',
    end = '',
  ): Promise<Array<DataElement<TransactionAttributes>>> {
    this.interceptor("getTransactionsForAccount")
    return new Promise<Array<DataElement<TransactionAttributes>>>((resolve, reject) => {
      FireflyIIIApi.getTransactionsForAccountRaw(this.apiUrl, this.token, accountId, type, page, limit, start, end)
        .then((res) => res.json())
        .catch((err) => reject(err))
        .then((json) => {
          let response = json as ApiResponse<TransactionAttributes>;
          resolve(response.data as Array<DataElement<TransactionAttributes>>);
        })
        .catch((err) => reject(err));
    });
  }

  public getTransactions(
    type: string,
    page: string,
    start: string,
    end: string,
  ): Promise<Array<DataElement<TransactionAttributes>>> {
    return new Promise<Array<DataElement<TransactionAttributes>>>((resolve, reject) => {
      this.interceptor("getTransactionsForAccount")
      FireflyIIIApi.getTransactionsRaw(this.apiUrl, this.token, type, page, start, end)
        .then((res) => res.json())
        .catch((err) => reject(err))
        .then((json) => {
          let response = json as ApiResponse<TransactionAttributes>;
          resolve(response.data as Array<DataElement<TransactionAttributes>>);
        })
        .catch((err) => reject(err));
    });
  }

  public addTransaction(transaction: TransactionStoreElement): Promise<Response> {
    this.interceptor("addTransaction")
    return fetch(`${this.apiUrl}/api/v1/transactions`, {
      method: 'POST',
      headers: FireflyIIIApi.generateHeaders(this.token, true),
      body: JSON.stringify(transaction),
    });
  }

  public getChartsAccountAssets(
    start: string = '',
    end: string = '',
  ): Promise<Array<ApiChartData>> {
    this.interceptor("getTransactionsForAccount")
    return new Promise<Array<ApiChartData>>((resolve, reject) => {
      FireflyIIIApi.getChartsAccountAssetsRaw(this.apiUrl, this.token, start, end)
        .then((res) => res.json())
        .catch((err) => reject(err))
        .then((json) => {
          resolve((json as unknown) as Array<ApiChartData>);
        })
        .catch((err) => reject(err));
    });
  }

  public getSummary(start: string, end: string): Promise<Summary> {
    this.interceptor("getTransactionsForAccount")
    return new Promise<Summary>((resolve, reject) => {
      FireflyIIIApi.getSummaryRaw(this.apiUrl, this.token, start, end)
        .then((res) => res.json())
        .catch((err) => reject(err))
        .then((json) => {
          resolve((json as unknown) as Summary);
        })
        .catch((err) => reject(err));
    });
  }

  public getCategories(page: number = 1): Promise<Array<DataElement<CategoryAttributes>>> {
    this.interceptor("getTransactionsForAccount")
    return new Promise<Array<DataElement<CategoryAttributes>>>((resolve, reject) => {
      FireflyIIIApi.getCategoriesRaw(this.apiUrl, this.token, page)
        .then((res) => res.json())
        .catch((err) => reject(err))
        .then((json) => {
          let response = json as ApiResponse<CategoryAttributes>;
          resolve(response.data);
        })
        .catch((err) => reject(err));
    });
  }

  public getTags(page: number = 1): Promise<Array<DataElement<TagAttributes>>> {
    this.interceptor("getTransactionsForAccount")
    return new Promise<Array<DataElement<TagAttributes>>>((resolve, reject) => {
      FireflyIIIApi.getTagsRaw(this.apiUrl, this.token, page)
        .then((res) => res.json())
        .catch((err) => reject(err))
        .then((json) => {
          let response = json as ApiResponse<TagAttributes>;
          resolve(response.data);
        })
        .catch((err) => reject(err));
    });
  }

  public getBudgets(page: number = 1, start: string = '', end: string = '',): Promise<Array<DataElement<BudgetAttributes>>> {
    this.interceptor("getTransactionsForAccount")
    return new Promise<Array<DataElement<BudgetAttributes>>>((resolve, reject) => {
      FireflyIIIApi.getBudgetsRaw(this.apiUrl, this.token, page, start, end)
        .then((res) => res.json())
        .catch((err) => reject(err))
        .then((json) => {
          let response = json as ApiResponse<BudgetAttributes>;
          resolve(response.data);
        })
        .catch((err) => reject(err));
    });
  }

  /*
   * Private
   */

  private static getBudgetsRaw(
    apiUrl: string,
    token: string,
    page: number = 1,
    start: string = '',
    end: string = '',
  ): Promise<Response> {
    let query = FireflyIIIApi.encodeQueryData({ page: JSON.stringify(page), start, end });
    return fetch(`${apiUrl}/api/v1/budgets?${query}`, {
      method: 'GET',
      headers: FireflyIIIApi.generateHeaders(token),
    });
  }

  private static getTagsRaw(apiUrl: string, token: string, page: number = 1): Promise<Response> {
    let query = FireflyIIIApi.encodeQueryData({ page: JSON.stringify(page) });
    return fetch(`${apiUrl}/api/v1/tags?${query}`, {
      method: 'GET',
      headers: FireflyIIIApi.generateHeaders(token),
    });
  }

  private static getAccountsRaw(apiUrl: string, token: string, type = ''): Promise<Response> {
    return fetch(`${apiUrl}/api/v1/accounts?type=${type}`, {
      method: 'GET',
      headers: FireflyIIIApi.generateHeaders(token),
    });
  }

  private static getTransactionsRaw(
    apiUrl: string,
    token: string,
    type: string,
    page: string,
    start: string,
    end: string,
  ): Promise<Response> {
    let query = FireflyIIIApi.encodeQueryData({ type, page, start, end });
    return fetch(`${apiUrl}/api/v1/transactions?${query}`, {
      method: 'GET',
      headers: FireflyIIIApi.generateHeaders(token),
    });
  }

  private static getChartsAccountAssetsRaw(apiUrl: string, token: string, start: string = '', end: string = '') {
    let query = FireflyIIIApi.encodeQueryData({ start, end });
    return fetch(`${apiUrl}/api/v1/chart/account/overview?${query}`, {
      method: 'GET',
      headers: FireflyIIIApi.generateHeaders(token),
    });
  }

  private static getSummaryRaw(apiUrl: string, token: string, start: string, end: string): Promise<Response> {
    let query = FireflyIIIApi.encodeQueryData({ start, end });
    return fetch(`${apiUrl}/api/v1/summary/basic?${query}`, {
      method: 'GET',
      headers: FireflyIIIApi.generateHeaders(token),
    });
  }

  private static getCategoriesRaw(apiUrl: string, token: string, page: number = 1): Promise<Response> {
    let query = FireflyIIIApi.encodeQueryData({ page: JSON.stringify(page) });
    return fetch(`${apiUrl}/api/v1/categories?${query}`, {
      method: 'GET',
      headers: FireflyIIIApi.generateHeaders(token),
    });
  }

  private static getTransactionsForAccountRaw(
    apiUrl: string,
    token: string,
    accountId = '1',
    type = 'all',
    page = '',
    limit = '',
    start = '',
    end = '',
  ): Promise<Response> {
    let query = FireflyIIIApi.encodeQueryData({ type, page, limit, start, end });
    return fetch(`${apiUrl}/api/v1/accounts/${accountId}/transactions?${query}`, {
      method: 'GET',
      headers: FireflyIIIApi.generateHeaders(token),
    });
  }

  /*
   * Utils
   */

  private static encodeQueryData(data: { [key: string]: string }) {
    const ret = [];
    for (let d in data) {
      ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
    }
    return ret.join('&');
  };

  private static generateHeaders(token: string, contentType = false) {
    let headers = {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    };
    if (contentType) {
      // @ts-ignore
      headers['Content-Type'] = 'application/json';
    }
    return headers;
  };

}
