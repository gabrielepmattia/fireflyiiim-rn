export interface SummaryItem {
  key: string;
  title: string;
  monetary_value: number,
  currency_id: string;
  currency_code: string;
  currency_symbol: string;
  currency_decimal_places: number;
  value_parsed: string;
  local_icon: string;
  sub_title: string;
}

export interface Summary {
  [key: string]: SummaryItem
}