export const TRANSACTION_TYPE_DEPOSIT = "deposit";
export const TRANSACTION_TYPE_WITHDRAWAL = "withdrawal";
export const TRANSACTION_TYPE_TRANSFER = "transfer";

export interface Transaction {
    user: string;
    transaction_journal_id: string;
    type: string;
    date: string;
    order: string;
    currency_id: string;
    currency_code: string;
    currency_name: string;
    currency_symbol: string;
    currency_decimal_places: string;
    foreign_currency_id: string;
    foreign_currency_code: string;
    foreign_currency_symbol: string;
    foreign_currency_decimal_places: string;
    amount: number;
    foreign_amount: string;
    description: string;
    source_id: string;
    source_name: string;
    source_iban: string;
    source_type: string;
    destination_id: string;
    destination_name: string;
    destination_iban: string;
    destination_type: string;
    budget_id: string;
    budget_name: string;
    category_id: string | null;
    category_name: string;
    bill_id: string;
    bill_name: string;
    reconciled: boolean;
    notes: string;
    tags: Array<string>,
    internal_reference: string;
    external_id: string;
    original_source: string;
    recurrence_id: string;
    bunq_payment_id: string;
    import_hash_v2: string;
    sepa_cc: string;
    sepa_ct_op: string;
    sepa_ct_id: string;
    sepa_db: string;
    sepa_country: string;
    sepa_ep: string;
    sepa_ci: string;
    sepa_batch_id: string;
    interest_date: string;
    book_date: string;
    process_date: string;
    due_date: string;
    payment_date: string;
    invoice_date: string;
}

export interface TransactionAttributes {
    created_at: string;
    updated_at: string;
    user: string;
    group_title: string;
    transactions: Array<Transaction>
}

export interface TransactionStoreElement {
    apply_rules: boolean;
    group_title: string;
    transactions: Array<Transaction>;
}
