export interface AboutAttributes {
  version: string;
  api_version: string;
  php_version: string;
  os: string;
  driver: string;
}