export interface CategoryAttributes {
  created_at: string;
  updated_at: string;
  name: string;
  spent: Array<CategoryAttributesMoney>;
  earned: Array<CategoryAttributesMoney>;
}

export interface CategoryAttributesMoney {
  start: string;
  end: string;
  currency_id: number;
  currency_code: string;
  currency_symbol: string;
  currency_decimal_places: number;
  amount: number;
}