import { AboutAttributes } from "./about";
import { DataElement, DataElementAttributesType } from "./data_elements";

export const API_DATE_FORMAT = "YYYY-MM-DD"

export interface ApiResponse<T extends DataElementAttributesType> {
  data: Array<DataElement<T>>;
  meta: ApiResponseMeta;
}

export interface ApiRead<T extends DataElementAttributesType> {
  data: DataElement<T>
}

export interface ApiResponseMeta {
  pagination: ApiResponseMetaPagination
}

export interface ApiResponseMetaPagination {
  total: number;
  count: number;
  per_page: number;
  current_page: number;
  total_pages: number;
}

export interface ApiResponseAbout {
  data: AboutAttributes
}