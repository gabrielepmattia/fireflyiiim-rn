export interface BudgetAttributes {
  created_at: string;
  updated_at: string;
  name: string;
  active: boolean;
  order: number;
  spent: Array<BudgedAttributesMoney>
}

export interface BudgedAttributesMoney {
  sum: number;
  currency_id: number;
  currency_code: string;
  currency_symbol: string;
  currency_decimal_places: number;
}