export interface ApiChartData {
  label: string;
  currency_id: number;
  currency_code: string;
  currency_symbol: string;
  currency_decimal_places: number;
  type: string;
  yAxisID: number;
  entries: ApiChartDataEntries;
}

export interface ApiChartDataEntries {
  [date: string]: number
}