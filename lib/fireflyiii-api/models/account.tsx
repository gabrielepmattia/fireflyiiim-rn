export const ACCOUNT_TYPE_ALL = "all"
export const ACCOUNT_TYPE_ASSET = "asset"
export const ACCOUNT_TYPE_CASH = "cash"
export const ACCOUNT_TYPE_EXPENSE = "expense"
export const ACCOUNT_TYPE_REVENUE = "revenue"
export const ACCOUNT_TYPE_SPECIAL = "special"
export const ACCOUNT_TYPE_HIDDEN = "hidden"
export const ACCOUNT_TYPE_LIABILITY = "liability"
export const ACCOUNT_TYPE_LIABILITIES = "liabilities"

export const ACCOUNT_ROLE_ASSET_DEFAULT = "defaultAsset"
export const ACCOUNT_ROLE_ASSET_CREDIT_CARD = "ccAsset"
export const ACCOUNT_ROLE_ASSET_CASH_WALLET = "cashWalletAsset"
export const ACCOUNT_ROLE_ASSET_SAVING = "savingAsset"


export interface AccountAttributes {
  created_at: string;
  updated_at: string;
  active: boolean;
  name: string;
  type: string;
  account_role: string;
  currency_id: number;
  currency_code: string;
  currency_symbol: string;
  currency_decimal_places: number;
  current_balance: number;
  current_balance_date: string;
  notes: string;
  monthly_payment_date: string;
  credit_card_type: string;
  account_number: string;
  iban: string;
  bic: string;
  virtual_balance: number;
  opening_balance: number;
  opening_balance_date: string;
  liability_type: string;
  liability_amount: number;
  liability_start_date: string;
  interest: number;
  interest_period: string;
  include_net_worth: boolean;
  longitude: string;
  latitude: string;
  zoom_level: string;
}

/*
 * Utils
 */

export const renderAccountTypeString = (type: string) => {
  switch (type) {
    case ACCOUNT_TYPE_CASH:
      return "Cash (Revenue)";
    case ACCOUNT_TYPE_ASSET:
      return "Asset";
    case ACCOUNT_TYPE_EXPENSE:
      return "Expense"
    case ACCOUNT_TYPE_REVENUE:
      return "Revenue"
    case ACCOUNT_TYPE_SPECIAL:
      return "Special"
    default:
      return type;
  }
}


export const renderAccountTypeRole = (type: string) => {
  switch (type) {
    case ACCOUNT_ROLE_ASSET_DEFAULT:
      return "Default";
    case ACCOUNT_ROLE_ASSET_CASH_WALLET:
      return "Cash Wallet";
    case ACCOUNT_ROLE_ASSET_CREDIT_CARD:
      return "Credit Card"
    case ACCOUNT_ROLE_ASSET_SAVING:
      return "Savings"
    default:
      return type;
  }
}
