export interface TagAttributes {
  created_at: string;
  updated_at: string;
  tag: string;
  date: string;
  description: string;
  latitude: number;
  longitude: number;
  zoom_level: number;
}