import { AccountAttributes } from "./account";
import { BudgetAttributes } from "./budget";
import { CategoryAttributes } from "./categories";
import { TagAttributes } from "./tags";
import { TransactionAttributes } from "./transaction";

export interface DataElement<T extends DataElementAttributesType> {
  type: string;
  id: string;
  attributes: T;
}

export type DataElementAttributesType = AccountAttributes | TransactionAttributes | CategoryAttributes | TagAttributes | BudgetAttributes;