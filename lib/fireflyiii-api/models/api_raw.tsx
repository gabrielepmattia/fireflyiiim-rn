export interface DemoCredentials {
  host: string;
  token: string;
}