import 'react-native-gesture-handler';

import React from 'react';
import * as eva from '@eva-design/eva';
// navigation
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// ui
import { ApplicationProvider, IconRegistry, StyleService, useStyleSheet } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
// state and data
import { connect, Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { default as customMapping } from './custom-mapping.json';
import { default as customTheme } from './custom-theme.json';
// screens
import LoginScreen from './src/screens/LoginScreen';
import HomeScreen from './src/screens/members/HomeScreen';

import { persistor, RootState, store } from './src/store/store';

const Stack = createStackNavigator();

/*
 * Root of the application
 */

const AppRoot = (): React.ReactElement => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <App/>
      </PersistGate>
    </Provider>
  );
};

export default AppRoot;

/*
 * App component with routes
 */

interface Props {
  signed_in: boolean;
}

const AppComponent = (props: Props): React.ReactElement => {
  const styles = useStyleSheet(themedStyles);

  const routes = (signed_in: boolean) => {
    return signed_in ? (
      <Stack.Screen name="Home" component={HomeScreen}/>
    ) : (
      <Stack.Screen name="Login" component={LoginScreen}/>
    );
  };

  return (
    <NavigationContainer independent={true}>
      <IconRegistry icons={EvaIconsPack}/>
      <ApplicationProvider {...eva} theme={{ ...eva.light, ...customTheme }} customMapping={customMapping}>
        <Stack.Navigator headerMode="none" screenOptions={{ cardStyle: styles.navigationCard }}>
          {routes(props.signed_in)}
        </Stack.Navigator>
      </ApplicationProvider>
    </NavigationContainer>
  );
};

const mapStateToProps = (state: RootState) => {
  return {
    signed_in: state.auth.signed_in,
  };
};

const App = connect(mapStateToProps)(AppComponent);

const themedStyles = StyleService.create({
  navigationCard: {
    // backgroundColor: "#fff" // TODO to fix
  },
});
