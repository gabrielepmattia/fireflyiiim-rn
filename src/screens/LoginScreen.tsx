import React, { Dispatch, useState } from 'react';

import {connect} from 'react-redux';
import { Button, Input, Layout, StyleService, Text, useStyleSheet, useTheme } from '@ui-kitten/components';
import { Linking, SafeAreaView, StatusBar, View } from 'react-native';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import { AboutAttributes } from 'lib/fireflyiii-api/models/about';
import { DemoCredentials } from 'lib/fireflyiii-api/models/api_raw';
import { FireflyIIIApi } from "../../lib/fireflyiii-api/api";
import { setAuthSignedIn, setAuthToken } from "../actions/auth";
import { setCacheAbout, setCacheClean } from "../actions/cache";
import { setConfigApiUrl } from "../actions/config";
import { RootState } from "../store/store";
import { StoreActionType } from "../store/types";
import { retrieveDemoCredentials } from "../utils/api";
import { CodeIcon, HardDriveIcon } from '../utils/icons';

export interface Props {
  api_url: string;
  token: string;

  onLogin: (apiUrl: string, token: string, about: AboutAttributes) => void;
}

interface DispatchFromProps {
  onLogin: (apiUrl: string, token: string, about: AboutAttributes) => void;
}

const LoginScreen = (props: Props): React.ReactElement => {
  const MODULE = "LoginScreen"

  const styles = useStyleSheet(themedStyles);
  const theme = useTheme();

  const [apiUrl, setApiUrl] = useState('');
  const [token, setToken] = useState('');
  const [loading, setLoading] = useState(false);

  const handleApiUrlChange = (newValue: string) => {
    setApiUrl(newValue);
  };

  const handleTokenChange = (newValue: string) => {
    setToken(newValue);
  };

  const login = () => {
    if (apiUrl === '' || token === '') {
      return;
    }

    // check if demo credentials are used
    setLoading(true);
    if (apiUrl === 'demo' && token === 'demo1234') {
      retrieveDemoCredentials()
        .then((res) => res.json())
        .catch((err) => console.error(`[${MODULE}] retrieveDemoCredential api error: ${err}`))
        .then((res) => {
          let cred = res as DemoCredentials;
          FireflyIIIApi.loginWithJWT(cred.host, cred.token)
            .then((res) => res.json())
            .then((res) => {
              props.onLogin(cred.host, cred.token, res.data as AboutAttributes);
              setLoading(false);
            })
            .catch((err) => {
              console.debug(`[${MODULE}] loginWithJWT: error ${err}`);
              setLoading(false);
            });
        });
      return;
    }

    FireflyIIIApi.loginWithJWT(apiUrl, token)
      .then((res) => res.json())
      .then((res) => {
        props.onLogin(apiUrl, token, res.data as AboutAttributes);
        setLoading(false);
      })
      .catch((err) => {
        console.debug(`[${MODULE}] loginWithJWT: fetch to ${apiUrl} error ${err}`);
        setLoading(false);
      });
  };

  const createToken = () => Linking.openURL('https://docs.firefly-iii.org/api/api#authentication');

  const keyboardAwareScrollViewProps = {
    style: styles.mainContainer,
    contentContainerStyle: { flexGrow: 1 },
    bounces: false,
    bouncesZoom: false,
    alwaysBounceVertical: false,
    alwaysBounceHorizontal: false,
  };

  /*
   * Component
   */

  return (
    <>
      <SafeAreaView style={{ flex: 0, backgroundColor: theme['color-primary-500'] }} />
      <StatusBar barStyle="light-content" backgroundColor="#3C8DBC" />
      <SafeAreaView style={{ flex: 1 }}>
        <KeyboardAwareScrollView {...keyboardAwareScrollViewProps}>
          <View style={styles.headerContainer}>
            <Text category="h1" status="control">
              Firefly III
            </Text>
            <Text style={styles.signInLabel} category="s1" status="control">
              Sign in with your Access Token
            </Text>
          </View>
          <Layout style={styles.formContainer}>
            <Input
              testID="input-url"
              textContentType="URL"
              value={apiUrl}
              disabled={loading}
              onChangeText={handleApiUrlChange}
              placeholder="Host address"
              accessoryRight={HardDriveIcon}
            />
            <Input
              testID="input-token"
              value={token}
              onChangeText={handleTokenChange}
              disabled={loading}
              multiline={true}
              numberOfLines={8}
              style={styles.passwordInput}
              placeholder="Personal Access Token"
              accessoryRight={CodeIcon}
            />
          </Layout>
          <Button testID="button-login" disabled={loading} style={styles.signInButton} onPress={login} size="giant">
            LOG IN
          </Button>
          <Button testID="button-help" style={styles.signUpButton} appearance="ghost" onPress={createToken} status="basic">
            Don't have a token? Create it!
          </Button>
        </KeyboardAwareScrollView>
      </SafeAreaView>
      <SafeAreaView style={{ flex: 0, backgroundColor: theme['background-basic-color-1'] }} />
    </>
  );
};

const mapStateToProps = (state: RootState) => {
  return {};
};

const mapDispatchToProps = (dispatch: Dispatch<StoreActionType>): DispatchFromProps => ({
  onLogin: (apiUrl: string, token: string, about: AboutAttributes) => {
    dispatch(setCacheClean());
    dispatch(setAuthToken(token));
    dispatch(setConfigApiUrl(apiUrl));
    dispatch(setAuthSignedIn(true));
    dispatch(setCacheAbout(about));
  },
});


export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

const themedStyles = StyleService.create({
  mainContainer: {
    flex: 1,
    backgroundColor: 'background-basic-color-1',
  },
  headerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 216, // 216,
    backgroundColor: 'color-primary-500',
  },
  formContainer: {
    flex: 1,
    paddingTop: 32,
    paddingHorizontal: 16,
  },
  signInLabel: {
    marginTop: 16,
  },
  signInButton: {
    marginTop: 16,
    marginHorizontal: 16,
  },
  signUpButton: {
    marginVertical: 12,
    marginHorizontal: 16,
  },
  forgotPasswordContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  passwordInput: {
    fontFamily: 'monospace',
    marginTop: 16,
    maxHeight: 150,
  },
  forgotPasswordButton: {
    paddingHorizontal: 0,
  },
});
