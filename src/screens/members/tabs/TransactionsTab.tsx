import { Divider, List, StyleService, TopNavigation, TopNavigationAction, useStyleSheet } from '@ui-kitten/components';
import React, { Dispatch } from 'react';
import { connect } from 'react-redux';
import { DataElement } from 'lib/fireflyiii-api/models/data_elements';
import { TransactionAttributes } from 'lib/fireflyiii-api/models/transaction';
import { BackIcon, MenuIcon, PlusIcon } from 'src/utils/icons';
import ModalService from "../../../services/modal.service";
import PopoverService from "../../../services/popover.service";
import { RootState } from '../../../store/store';
import { StoreActionType } from '../../../store/types';
import ModalPanelInputComponent from "../components/modals/ModalPanelInputComponent";
import { AddTransactionPopoverPanel, PopoverPanelStyle } from "../components/popovers/AddTransactionPopoverPanel";
import TransactionListItem from '../components/TransactionListItem';

const MODULE = "TransactionTab"

/*
 * Interfaces
 */

export interface Props {
  cachedTransactions: Array<DataElement<TransactionAttributes>>;
}

interface DispatchFromProps {
}

/*
 * Components
 */

const TransactionsTab = (props: Props) => {
  const styles = useStyleSheet(themedStyles);

  /*
   * Renders
   */

  const renderTransactionListItem = (item: any) => {
    let transaction = item.item as DataElement<TransactionAttributes>;
    return <TransactionListItem transactionItem={transaction}/>;
  };

  const renderTopNavigationBackAction = () =>
    <TopNavigationAction icon={BackIcon} onPress={() => props.navigation.goBack()}/>;

  const renderTopNavigationMenuAction = () =>
    <TopNavigationAction icon={MenuIcon}/>;

  const renderTopNavigationRightActions = () => (
    <React.Fragment>
      <TopNavigationAction
        icon={PlusIcon}
        onPress={() => {
          // props.navigation.navigate(ROUTE_ADD_TRANSACTION)
          PopoverService.getInstance().setPopoverPanel(AddTransactionPopoverPanel)
          PopoverService.getInstance().setPopoverPanelStyle(PopoverPanelStyle)
          PopoverService.getInstance().show(true)
          console.debug(`[${MODULE}] Clicked on add transaction`)
        }}
      />
    </React.Fragment>
  );

  return (
    <>
      <TopNavigation accessoryRight={renderTopNavigationRightActions} alignment="center" title="Transactions"/>
      <Divider/>
      <List data={props.cachedTransactions} renderItem={renderTransactionListItem}/>
    </>
  );
};

/*
 * Store
 */

const mapStateToProps = (state: RootState) => ({
  cachedTransactions: state.cache.dashboard.transactions,
});

const mapDispatchToProps = (dispatch: Dispatch<StoreActionType>): DispatchFromProps => ({});

/*
 * Export
 */

export default connect(mapStateToProps, mapDispatchToProps)(TransactionsTab);

/*
 * Styles
 */

const themedStyles = StyleService.create({});
