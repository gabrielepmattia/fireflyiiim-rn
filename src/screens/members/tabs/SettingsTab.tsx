import { Divider, Layout, StyleService, TopNavigation, useStyleSheet, useTheme } from '@ui-kitten/components';
import Form from 'lib/react-native-fields/components/Form';
import {
  FIELD_TYPE_BUTTON,
  FIELD_TYPE_CAPTION,
  FIELD_TYPE_FIXED_TEXT,
  FIELD_TYPE_HEADER,
  FIELD_TYPE_PADDER,
  FieldItemButton,
  FieldItemCaption,
  FieldItemFixedText,
  FieldItemHeader,
  FieldItemPadder,
} from 'lib/react-native-fields/types/fields';
import React, { Dispatch } from 'react';
import { Linking } from 'react-native';
import { connect } from 'react-redux';
import { setAuthSignedIn, setAuthToken } from 'src/actions/auth';
import { setCacheClean } from 'src/actions/cache';
import { AboutAttributes } from 'lib/fireflyiii-api/models/about';
import { default as customMapping } from '../../../../custom-mapping.json';
import { default as pkg } from '../../../../package.json';
import { ROUTE_DEBUG } from "../../../routes/routesHome";
import { RootState } from '../../../store/store';
import { StoreActionType } from '../../../store/types';

/*
 * Interfaces
 */

export interface Props {
  // mapped
  serverInfo: AboutAttributes;
  // dispatch
  onLogout: () => void;
}

interface DispatchFromProps {
  onLogout: () => void;
}

/*
 * Component
 */

const SettingsTab = (props: Props) => {
  const styles = useStyleSheet(themedStyles);
  const theme = useTheme();

  const fieldsList = [
    {
      id: 'app-header',
      type: FIELD_TYPE_HEADER,
      title: 'App',
    } as FieldItemHeader,
    {
      id: 'logout',
      type: FIELD_TYPE_BUTTON,
      title: 'Log out',
      onPressed: () => {
        props.onLogout();
      },
    } as FieldItemButton,
    {
      id: 'debug',
      type: FIELD_TYPE_BUTTON,
      title: 'Debug',
      onPressed: () => {
        props.navigation.navigate(ROUTE_DEBUG);
      },
    } as FieldItemButton,
    {
      id: 'logout-caption',
      type: FIELD_TYPE_CAPTION,
      text: 'If you want to change the server address you need to log out and log in again',
    } as FieldItemCaption,
    {
      id: 'version',
      type: FIELD_TYPE_FIXED_TEXT,
      title: 'Version',
      value: pkg.version,
    } as FieldItemFixedText,
    {
      id: 'header-server',
      type: FIELD_TYPE_HEADER,
      title: 'Server',
    } as FieldItemHeader,
    {
      id: 'server-version',
      type: FIELD_TYPE_FIXED_TEXT,
      title: 'Version',
      value: props.serverInfo.version,
    } as FieldItemFixedText,
    {
      id: 'api-version',
      type: FIELD_TYPE_FIXED_TEXT,
      title: 'API Version',
      value: props.serverInfo.api_version,
    } as FieldItemFixedText,
    {
      id: 'php-version',
      type: FIELD_TYPE_FIXED_TEXT,
      title: 'PHP Version',
      value: props.serverInfo.php_version,
    } as FieldItemFixedText,
    {
      id: 'credits-header',
      type: FIELD_TYPE_HEADER,
      title: 'Credits',
    } as FieldItemHeader,
    {
      id: 'credits-name',
      type: FIELD_TYPE_BUTTON,
      title: '@gabrielepmattia',
      onPressed: () => {
        Linking.openURL('https://gabrielepmattia.com');
      },
    } as FieldItemButton,
    {
      id: 'credits-paypal',
      type: FIELD_TYPE_BUTTON,
      title: 'Donate with PayPal',
      onPressed: () => {
        Linking.openURL('https://paypal.me/gabrielepmattia');
      },
    } as FieldItemButton,
    {
      id: 'credits-patreon',
      type: FIELD_TYPE_BUTTON,
      title: 'Become my Patron',
      onPressed: () => {
        Linking.openURL('https://patreon.com/gabrielepmattia');
      },
    } as FieldItemButton,
    {
      id: 'credits-patreon-patreon',
      type: FIELD_TYPE_CAPTION,
      text: 'If you like my work you can support me, this app will be always ad-free. Thank you!',
    } as FieldItemCaption,
    {
      id: 'credits-feedback',
      type: FIELD_TYPE_BUTTON,
      title: 'Send feedback or suggestions',
      onPressed: () => {
        Linking.openURL('https://forms.gle/o95DNBpax88mdDyy5');
      },
    } as FieldItemButton,
    {
      id: 'padder-final',
      type: FIELD_TYPE_PADDER,
      height: 32,
    } as FieldItemPadder,
  ];

  return (
    <>
      <TopNavigation alignment="center" title="Settings" />
      <Divider />
      <Layout level="2" style={{ flex: 1 }}>
        <Form fieldItems={fieldsList} theme={theme} customMapping={customMapping} />
      </Layout>
    </>
  );
};

/*
 * Store
 */

const mapStateToProps = (state: RootState) => ({
  serverInfo: state.cache.about,
});

const mapDispatchToProps = (dispatch: Dispatch<StoreActionType>): DispatchFromProps => ({
  onLogout: () => {
    dispatch(setCacheClean());
    dispatch(setAuthToken(''));
    dispatch(setAuthSignedIn(false));
  },
});

/*
 * Export
 */

export default connect(mapStateToProps, mapDispatchToProps)(SettingsTab);

/*
 * Styles
 */

const themedStyles = StyleService.create({});
