import React from 'react';
import { Dispatch } from "react";
import { connect } from "react-redux";
import { StyleService, useStyleSheet } from "@ui-kitten/components";

import { RootState } from "../../../store/store";
import { StoreActionType } from "../../../store/types";

/*
 * Interfaces
 */

export interface Props {
}

interface DispatchFromProps {
}

/*
 * Component
 */

const MoneyManagementTab = () => {

  return (
    <></>
  )

}

/*
 * Store
 */

const mapStateToProps = (state: RootState) => ({

});

const mapDispatchToProps = (dispatch: Dispatch<StoreActionType>): DispatchFromProps => ({

});

/*
 * Export
 */

export default connect(mapStateToProps, mapDispatchToProps)(MoneyManagementTab);

/*
 * Styles
 */

const themedStyles = StyleService.create({

});