import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { NavigationProp, RouteProp } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {
  Divider,
  Icon,
  List,
  ListItem,
  Tab,
  TabBar,
  Text,
  TopNavigation,
  TopNavigationAction,
  useStyleSheet,
  useTheme
} from "@ui-kitten/components";
import { accounting } from "accounting";
import React, { useState } from 'react';
import { View, ViewProps } from 'react-native';
import { connect } from "react-redux";
import { DataElement } from 'lib/fireflyiii-api/models/data_elements';
import {
  ROUTE_ACCOUNT_INFO,
  ROUTE_HOME_ACCOUNTS_TAB_ASSETS,
  ROUTE_HOME_ACCOUNTS_TAB_EXPENSE,
  ROUTE_HOME_ACCOUNTS_TAB_LIABILITIES,
  ROUTE_HOME_ACCOUNTS_TAB_REVENUE
} from 'src/routes/routesHome';
import {
  AccountAttributes,
  ACCOUNT_TYPE_ASSET,
  ACCOUNT_TYPE_EXPENSE,
  ACCOUNT_TYPE_LIABILITIES,
  ACCOUNT_TYPE_REVENUE,
  renderAccountTypeString,
  renderAccountTypeRole
} from '../../../../lib/fireflyiii-api/models/account';
import { RootState } from "../../../store/store";
import { MenuIcon } from '../../../utils/icons';


/*
 * Interfaces
 */

interface Props {
  route: RouteProp<any, 'Accounts'>;
  navigation: NavigationProp<any, 'Accounts'>;

  // mapped props
  api_url: string;
  token: string;
  cachedAccounts: Array<DataElement<AccountAttributes>>
}

interface AccountsScreenProps {
  accountsType: string;
  // inherited props
  route: RouteProp<any, 'Accounts'>;
  navigation: NavigationProp<any, 'Accounts'>;
  // cache
  accountsList: Array<DataElement<AccountAttributes>>
}

interface DispatchFromProps {
}

export interface DisplayAccount {
  id: string;
  name: string;
  current_balance: number;
  currency_symbol: string;
  type: string;
  role: string;
}

enum AccountListItemType {
  ACCOUNT, HEADER
}

interface AccountListItem<T> {
  type: AccountListItemType
  value: T
}

const MODULE = "AccountsTab"

/*
 * Account Tab
 */

const AccountsTab = (props: Props) => {

  const [accountsListItems, setAccountsListItems] = useState([])

  React.useEffect(() => {
    console.debug(`[${MODULE}] useEffect: started`)
    let accountsList = []
    let assetsHeader = { type: AccountListItemType.HEADER, value: "Assets" } as AccountListItem<string>
    let assets = props.cachedAccounts.filter((account) => account.attributes.type == ACCOUNT_TYPE_ASSET)
      .map((value: DataElement<AccountAttributes>) => {
        return { type: AccountListItemType.ACCOUNT, value } as AccountListItem<DataElement<AccountAttributes>>
      })
    let expenseHeader = { type: AccountListItemType.HEADER, value: "Expense" } as AccountListItem<string>
    let expense = props.cachedAccounts.filter((account) => account.attributes.type == ACCOUNT_TYPE_EXPENSE)
      .map((value: DataElement<AccountAttributes>) => {
        return { type: AccountListItemType.ACCOUNT, value } as AccountListItem<DataElement<AccountAttributes>>
      })
    let revenueHeader = { type: AccountListItemType.HEADER, value: "Revenue" } as AccountListItem<string>
    let revenue = props.cachedAccounts.filter((account) => account.attributes.type == ACCOUNT_TYPE_REVENUE)
      .map((value: DataElement<AccountAttributes>) => {
        return { type: AccountListItemType.ACCOUNT, value } as AccountListItem<DataElement<AccountAttributes>>
      })
    let liabilitiesHeader = { type: AccountListItemType.HEADER, value: "Liabilities" } as AccountListItem<string>
    let liabilities = props.cachedAccounts.filter((account) => account.attributes.type == ACCOUNT_TYPE_LIABILITIES)
      .map((value: DataElement<AccountAttributes>) => {
        return { type: AccountListItemType.ACCOUNT, value } as AccountListItem<DataElement<AccountAttributes>>
      })
    if (assets.length > 0) {
      accountsList.push(assetsHeader)
      accountsList = accountsList.concat(assets)
    }
    if (expense.length > 0) {
      accountsList.push(expenseHeader)
      accountsList = accountsList.concat(expense)
    }
    if (revenue.length > 0) {
      accountsList.push(revenueHeader)
      accountsList = accountsList.concat(revenue)
    }
    if (liabilities.length > 0) {
      accountsList.push(liabilitiesHeader)
      accountsList = accountsList.concat(liabilities)
    }
    setAccountsListItems(accountsList)
    console.debug(`[${MODULE}] useEffect: set ${accountsList.length} items`)
  }, [props.cachedAccounts])

  const theme = useTheme();
  const styles = useStyleSheet({
    accountItem: {
      paddingLeft: 16,
      paddingRight: 8
    },
    accountBalance: {
      fontSize: 14
    },
    itemChevronIcon: {
      width: 24,
      height: 24,
    }
  });

  /*
   * Renders
   */

  /*
   * Subcomponents
   */

  const renderAccountsListItem = (item: any) => {
    let listItem = item.item as AccountListItem<string>
    if (listItem.type === AccountListItemType.HEADER) {
      return (<Text category="h2" style={{paddingHorizontal: 16, paddingVertical: 16}}>{listItem.value as string}</Text>)
    } else {
      let listItem_new = item.item as AccountListItem<DataElement<AccountAttributes>>
      let account = listItem_new.value as DataElement<AccountAttributes>
      return (
        <>
          <ListItem
            style={styles.accountItem}
            title={account.attributes.name}
            // description={renderAccountTypeRole(account.attributes.account_role)}
            accessoryRight={renderAccountBalance(account)}
            onPress={() => props.navigation.navigate(ROUTE_ACCOUNT_INFO, { account })}
          />
          <Divider/>
        </>
      )
    }
  }

  const renderAccountBalance = (account: DataElement<AccountAttributes>) => {
    return (viewProps: ViewProps) => {
      return (
        <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
          <Text status={account.attributes.current_balance < 0 ? 'danger' : 'success'} style={styles.accountBalance}>
            {accounting.formatMoney(account.attributes.current_balance, account.attributes.currency_symbol)}
          </Text>
          <Icon
            style={styles.itemChevronIcon}
            fill={theme['color-basic-500']}
            name='chevron-right'
          />
        </View>
      )
    }
  }

  /*
   * Component
   */

  return (
    <>
      <TopNavigation alignment='center' title='Accounts'/>
      <Divider/>
      <List data={accountsListItems} renderItem={renderAccountsListItem}/>
    </>
  )
}

/*
 *  -> Store
 */

const mapStateToProps = (state: RootState) => ({
  api_url: state.config.api_url,
  token: state.auth.token,
  // cache
  cachedAccounts: state.cache.accounts,
});

/*
 *  -> Export
 */

export default connect(mapStateToProps)(AccountsTab);
