import {
  Card,
  Divider,
  List,
  StyleService,
  Text,
  TopNavigation,
  TopNavigationAction,
  useStyleSheet,
} from '@ui-kitten/components';
import moment from 'moment';
import React, { Dispatch, useState } from 'react';
import { Dimensions, RefreshControl, View } from 'react-native';
import { LineChart } from 'react-native-chart-kit';
import { connect } from 'react-redux';
import { AccountAttributes } from 'lib/fireflyiii-api/models/account';
import { DataElement } from 'lib/fireflyiii-api/models/data_elements';
import { TransactionAttributes } from 'lib/fireflyiii-api/models/transaction';
import {
  refreshAccounts,
  refreshBudgets,
  refreshCategories,
  refreshDashboardSummary,
  refreshDashboardTransactions,
  refreshTags,
} from 'src/store/utils';
import { FireflyIIIApi } from "../../../../lib/fireflyiii-api/api";
import { Summary } from '../../../../lib/fireflyiii-api/models/summary';
import ApiService from "../../../services/api.service";
import { RootState } from '../../../store/store';
import { StoreActionType } from '../../../store/types';
import { CalendarIcon } from '../../../utils/icons';
import HorizontalSummaryCards from '../components/HorizontalSummaryCards';
import ListPadder from '../components/ListPadder';
import TransactionListItem from '../components/TransactionListItem';

const MODULE = "DashboardTab"

/*
 * Interfaces
 */

interface Props {
  api_url: string;
  token: string;

  // cache
  cachedAccounts: Array<DataElement<AccountAttributes>>;
  cachedSummary: Summary;
  cachedTransactions: Array<DataElement<TransactionAttributes>>;
}

interface DispatchFromProps {
}

export interface TransactionItem {
  index: number;
  total: number;

  type: string;
  title: string;
  date: string;
  value: number;
  currency_symbol: string;
}

/*
 * Component
 */

const DashboardTab = (props: Props) => {
  const styles = useStyleSheet(themedStyles);

  const [loadedData, setLoadedData] = useState(false);

  const [refreshing, setRefreshing] = useState(false)

  const [dateReportStart, setDateReportStart] = useState(moment().subtract(30, 'days'));
  const [dateSmallReportStart, setDateSmallReportStart] = useState(moment().subtract(7, 'days'));
  const [dateReportEnd, setDateReportEnd] = useState(moment());

  const [chartDisplayData, setChartDisplayData] = useState({
    labels: ['January', 'February', 'March', 'April', 'May', 'June'],
    datasets: [
      {
        data: [
          Math.random() * 100,
          Math.random() * 100,
          Math.random() * 100,
          Math.random() * 100,
          Math.random() * 100,
          Math.random() * 100,
        ],
      },
    ],
  });

  const dashboardItems = ['padder', 'main-title', 'infoboxes', 'chart', 'transactions', 'padder'];

  React.useEffect(() => {
    if (!loadedData) {
      loadData();
      setLoadedData(true);
    }
  }, []);

  const loadData = () => {
    console.debug(`[${MODULE}] started loading data`);
    // reload accounts to cache
    let startDate = dateReportStart.format('YYYY-MM-DD');
    let endDate = dateReportEnd.format('YYYY-MM-DD');
    let startDateSmall = dateSmallReportStart.format('YYYY-MM-DD');

    refreshDashboardSummary(startDate, endDate);
    refreshDashboardTransactions('', '1', startDate, endDate);

    ApiService.getInstance().handler().getChartsAccountAssets(startDateSmall, endDate)
      .then((charts) => {
        let labels = [];
        let datasets = [];

        for (let key in charts[0].entries) {
          labels.push(moment(key).format('DD'));
        }

        charts.forEach((chart) => {
          let dataset = { data: [] };
          for (let key in chart.entries) {
            dataset.data.push(chart.entries[key]);
          }
          datasets.push(dataset);
        });
        setChartDisplayData({ labels, datasets });
        console.debug(`[${MODULE}] apiGetChartsAccountAssets: done`);
      })
      .catch((err) => console.error(`[${MODULE}] apiGetChartsAccountAssets: api error: ${JSON.stringify(err)}`));

    refreshAccounts();
    refreshCategories();
    refreshTags();
    refreshBudgets();
  };

  /*
   * Renders
   */

  const renderDashboardListItem = (item: any) => {
    switch (item.item) {
      case 'padder':
        return <ListPadder/>;
      case 'main-title':
        return (
          <View style={styles.widgetContainer}>
            <Text category="h1">{`${dateReportStart.format('MMM DD')} - ${dateReportEnd.format('MMM DD')}`}</Text>
          </View>
        );
      case 'infoboxes':
        return <HorizontalSummaryCards summary={props.cachedSummary}/>;
      case 'chart':
        return (
          <>
            <View style={styles.widgetContainer}>
              <LineChart
                data={chartDisplayData}
                width={Dimensions.get('window').width - 16 * 2} // from react-native
                height={220}
                yAxisLabel="€"
                yAxisSuffix=""
                yAxisInterval={1} // optional, defaults to 1
                chartConfig={{
                  backgroundColor: '#e26a00',
                  backgroundGradientFrom: '#fb8c00',
                  backgroundGradientTo: '#ffa726',
                  decimalPlaces: 0, // optional, defaults to 2dp
                  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                  labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                  style: {
                    borderRadius: 16,
                  },
                  propsForDots: {
                    r: '4',
                    strokeWidth: '2',
                    stroke: '#ffa726',
                  },
                }}
                bezier
                style={{
                  marginVertical: 8,
                  borderRadius: 8,
                }}
              />
            </View>
          </>
        );
      case 'transactions':
        return (
          <View style={styles.widgetContainer}>
            <Text category="h2" style={styles.cardTitle}>
              Latest Transactions
            </Text>
            <Card appearance="filled" style={styles.cardContainer}>
              {props.cachedTransactions !== undefined && props.cachedTransactions.length > 0 && (
                <List data={props.cachedTransactions} renderItem={renderTransactionListItem}/>
              )}
              {props.cachedTransactions !== undefined && props.cachedTransactions.length === 0 && (
                <Text category="p1" style={styles.noTransactionAlert}>
                  There are no transactions in the selected period
                </Text>
              )}
            </Card>
          </View>
        );
    }
  };

  const renderTransactionListItem = (item: any) => {
    const transactionItem = item.item as DataElement<TransactionAttributes>;
    return (
      <TransactionListItem transactionItem={transactionItem}
                           showDivider={item.index < props.cachedTransactions.length - 1}/>
    );
  };

  const renderTopNavigationRightActions = () => (
    <React.Fragment>
      <TopNavigationAction icon={CalendarIcon}/>
    </React.Fragment>
  );

  /*
   * Component
   */

  return (
    <>
      <TopNavigation
        style={styles.topNavigation}
        alignment="center"
        title="Dashboard"
        // accessoryLeft={renderBackAction}
        accessoryRight={renderTopNavigationRightActions}
      />
      <Divider/>
      <List refreshControl={<RefreshControl refreshing={refreshing} onRefresh={() => {
        loadData()
      }}/>} style={styles.mainListContainer} data={dashboardItems} renderItem={renderDashboardListItem}/>
    </>
  );
};

/*
 * Store
 */

const mapStateToProps = (state: RootState) => ({
  api_url: state.config.api_url,
  token: state.auth.token,
  // cached
  cachedAccounts: state.cache.accounts,
  cachedSummary: state.cache.dashboard.summary,
  cachedTransactions: state.cache.dashboard.transactions,
});

const mapDispatchToProps = (dispatch: Dispatch<StoreActionType>): DispatchFromProps => ({});

/*
 * Export
 */

export default connect(mapStateToProps, mapDispatchToProps)(DashboardTab);

/*
 * Styles
 */

const themedStyles = StyleService.create({
  widgetContainer: {
    paddingHorizontal: 16,
    paddingVertical: 4,
  },
  topNavigation: {},
  mainListContainer: {},
  sectionHeader: {
    fontWeight: 'bold',
    marginTop: 16,
    marginBottom: 16,
  },
  verticalCardsList: {},
  cardContainer: {},
  cardOvertitle: {
    textTransform: 'uppercase',
    color: 'color-basic-600',
  },
  cardTitle: {
    fontWeight: 'bold',
    paddingBottom: 8,
  },
  noTransactionAlert: {
    color: '$text-hint-color',
    textAlign: 'center',
    padding: 16,
  },
});
