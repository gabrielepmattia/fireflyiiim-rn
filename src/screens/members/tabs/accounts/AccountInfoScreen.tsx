import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { NavigationProp, RouteProp } from '@react-navigation/native';
import { Divider, MenuItem, OverflowMenu, StyleService, Tab, TabBar, TopNavigation, TopNavigationAction, useStyleSheet } from "@ui-kitten/components";
import React, { Dispatch } from 'react';
import { BackHandler } from 'react-native';
import { connect } from "react-redux";
import { AccountAttributes, renderAccountTypeRole, renderAccountTypeString } from 'lib/fireflyiii-api/models/account';
import { DataElement } from 'lib/fireflyiii-api/models/data_elements';
import { ROUTE_HOME_ACCOUNTS, ROUTE_ACCOUNT_INFO, ROUTE_ADD_TRANSACTION, } from 'src/routes/routesHome';
import { BackIcon, InfoIcon, LogoutIcon, MenuIcon, PlusIcon } from "src/utils/icons";
import { RootState } from "../../../../store/store";
import { StoreActionType } from "../../../../store/types";
import AccountInfoSettingsTab from './AccountInfoSettingsTab';
import AccountInfoTransactionsTab from './AccountInfoTransactionsTab';


/*
 * Interfaces
 */

interface AccountInfoScreenRouteProps {
  [params: string]: {
    screen: string;
    account: DataElement<AccountAttributes>,
    fromAccount: string,
  };
}

export interface Props {
  route: RouteProp<AccountInfoScreenRouteProps, typeof ROUTE_ACCOUNT_INFO>;
  navigation: NavigationProp<AccountInfoScreenRouteProps, typeof ROUTE_ACCOUNT_INFO>;
}

interface DispatchFromProps {
}

/*
 * Component
 */

const AccountInfoScreen = (props: Props) => {

  const styles = useStyleSheet(themedStyles);

  React.useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", () => { props.navigation.goBack(); return true })
  }, [])

  /*
   * Renders
   */

  const renderTopNavigationBackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={() => props.navigation.goBack()} />
  );

  const renderTopNavigationMenuAction = () => (
    <TopNavigationAction icon={MenuIcon} />
  );

  const renderTopNavigationRightActions = () => (
    <React.Fragment>
      <TopNavigationAction icon={PlusIcon} onPress={() =>
        props.navigation.navigate(ROUTE_ADD_TRANSACTION, { fromAccount: props.route.params.id })
      } />
      {/*
      <OverflowMenu
        anchor={renderTopNavigationMenuAction}
        visible={false}
      // onBackdropPress={toggleMenu}
      >
        <MenuItem accessoryLeft={InfoIcon} title='About' />
        <MenuItem accessoryLeft={LogoutIcon} title='Logout' />
      </OverflowMenu>
      */}
    </React.Fragment>
  );

  /*
   * Subcomponents
   */

  const TopTabBar = ({ navigation, state }) => (
    <>
      <TabBar
        selectedIndex={state.index}
        onSelect={index => navigation.navigate(state.routeNames[index])}>
        <Tab title='Transactions' />
        <Tab title='Settings' />
      </TabBar>
      <Divider/>
    </>
  );

  const TabNavigator = createMaterialTopTabNavigator();

  return (
    <>
      <TopNavigation
        alignment="start"
        title={props.route.params?.account.attributes.name}
        subtitle={renderAccountTypeRole(props.route.params?.account.attributes.account_role)}
        accessoryLeft={renderTopNavigationBackAction}
        accessoryRight={renderTopNavigationRightActions}
      />
      <TabNavigator.Navigator backBehavior="none" tabBar={props => <TopTabBar {...props} />}>
        <TabNavigator.Screen name='Transactions'>
          {routeProps => <AccountInfoTransactionsTab {...routeProps} {...props} account={props.route.params.account} />}
        </TabNavigator.Screen>
        <TabNavigator.Screen name='Settings'>
          {routeProps => <AccountInfoSettingsTab {...routeProps} {...props} account={props.route.params.account} />}
        </TabNavigator.Screen>
      </TabNavigator.Navigator>
      <Divider></Divider>
    </>
  )

}

/*
 * Store
 */

const mapStateToProps = (state: RootState) => ({

});

const mapDispatchToProps = (dispatch: Dispatch<StoreActionType>): DispatchFromProps => ({

});

/*
 * Export
 */

export default connect(mapStateToProps, mapDispatchToProps)(AccountInfoScreen);

/*
 * Styles
 */

const themedStyles = StyleService.create({

});
