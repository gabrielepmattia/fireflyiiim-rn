import { List, StyleService, useStyleSheet } from "@ui-kitten/components";
import React, { Dispatch, useState } from 'react';
import { connect } from "react-redux";
import { DataElement } from "lib/fireflyiii-api/models/data_elements";
import { TransactionAttributes } from "lib/fireflyiii-api/models/transaction";
import { RootState } from "src/store/store";
import { StoreActionType } from "src/store/types";
import { FireflyIIIApi } from "../../../../../lib/fireflyiii-api/api";
import ApiService from "../../../../services/api.service";
import TransactionListItem from "../../components/TransactionListItem";
import { AccountAttributes } from "lib/fireflyiii-api/models/account";


/*
 * Interfaces
 */

export interface Props {
  account: DataElement<AccountAttributes>;
  // redux
  api_url: string;
  token: string;
}

interface DispatchFromProps {
}

/*
 * Component
 */

const AccountInfoTransactionTab = (props: Props) => {

  const styles = useStyleSheet(themedStyles);

  const [accountTransactions, setAccountTransactions] = useState(new Array<DataElement<TransactionAttributes>>());

  React.useEffect(() => {
    console.log(`AccountInfoTransactionTab: downloading transactions for account#${props.account.id}`)

    ApiService.getInstance().handler().getTransactionsForAccount(props.account.id)
      .then(transactionList => {
        setAccountTransactions(transactionList)
      })
  }, [props.account.id])

  /*
   * Renders
   */

  const renderTransactionListItem = (item: any) => {
    let transaction = item.item as DataElement<TransactionAttributes>
    return (
      <TransactionListItem transactionItem={transaction}/>
    )
  }

  /*
   * Component
   */

  return (
    <>
      <List data={accountTransactions} renderItem={renderTransactionListItem}/>
    </>
  )

}

/*
 * Store
 */

const mapStateToProps = (state: RootState) => ({
  api_url: state.config.api_url,
  token: state.auth.token
});

const mapDispatchToProps = (dispatch: Dispatch<StoreActionType>): DispatchFromProps => ({});

/*
 * Export
 */

export default connect(mapStateToProps, mapDispatchToProps)(AccountInfoTransactionTab);

/*
 * Styles
 */

const themedStyles = StyleService.create({});
