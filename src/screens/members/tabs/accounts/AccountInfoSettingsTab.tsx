import { List, StyleService, useStyleSheet } from "@ui-kitten/components";
import React, { Dispatch, useState } from 'react';
import { connect } from "react-redux";
import { DataElement } from "lib/fireflyiii-api/models/data_elements";
import { TransactionAttributes } from "lib/fireflyiii-api/models/transaction";
import { RootState } from "src/store/store";
import { StoreActionType } from "src/store/types";
import { DisplayAccount } from "../AccountsTab";



/*
 * Interfaces
 */

export interface Props {
  account: DisplayAccount;
  // redux
  api_url: string;
  token: string;
}

interface DispatchFromProps {
}

/*
 * Component
 */

const AccountInfoSettingsTab = (props: Props) => {

  const styles = useStyleSheet(themedStyles);

  const [accountTransactions, setAccountTransactions] = useState(new Array<DataElement<TransactionAttributes>>());

  React.useEffect(() => {
  }, [props.account.id])

  /*
   * Renders
   */

  const renderTransactionListItem = (item: any) => {
    let transaction = item.item as DataElement<TransactionAttributes>
    return (
      <></>
    )
  }

  /*
   * Component
   */

  return (
    <>
      <List data={accountTransactions} renderItem={renderTransactionListItem}></List>
    </>
  )

}

/*
 * Store
 */

const mapStateToProps = (state: RootState) => ({
  api_url: state.config.api_url,
  token: state.auth.token
});

const mapDispatchToProps = (dispatch: Dispatch<StoreActionType>): DispatchFromProps => ({

});

/*
 * Export
 */

export default connect(mapStateToProps, mapDispatchToProps)(AccountInfoSettingsTab);

/*
 * Styles
 */

const themedStyles = StyleService.create({

});
