import { Card, StyleService, Text, useStyleSheet } from '@ui-kitten/components';
import React from 'react';

interface Props {
  title: string;
  value: string;
  subtitle: string;
}

const InfoCard = (props: Props): React.ReactElement => {
  const styles = useStyleSheet(themedStyles);

  React.useEffect(() => {
    // console.log(props)
  }, []);

  return (
    <Card appearance="filled" style={styles.infoCard}>
      <Text style={styles.cardTitle}>{props.title}</Text>
      <Text style={styles.cardValue}>{props.value}</Text>
      {/* <Text style={styles.cardSubititle}>{props.subtitle}</Text> */}
    </Card>
  );
};

export default InfoCard;

const themedStyles = StyleService.create({
  infoCard: {
    paddingHorizontal: 16,
    paddingVertical: 10,
    marginRight: 16,
  },
  cardTitle: {
    fontSize: 14,
  },
  cardValue: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  cardSubititle: {
    fontSize: 12,
  },
});
