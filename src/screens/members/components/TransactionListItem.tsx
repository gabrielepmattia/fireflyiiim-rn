import { Divider, Icon, ListItem, StyleService, Text, useStyleSheet } from '@ui-kitten/components';
import moment from 'moment';
import React from 'react';
import { ViewProps } from 'react-native';
import { DataElement } from 'lib/fireflyiii-api/models/data_elements';
import { TransactionAttributes, TRANSACTION_TYPE_DEPOSIT, TRANSACTION_TYPE_TRANSFER, TRANSACTION_TYPE_WITHDRAWAL } from '../../../../lib/fireflyiii-api/models/transaction';

export interface TransactionListItemProps {
  transactionItem: DataElement<TransactionAttributes>
  showDivider?: boolean
}

const TransactionListItem = (props: TransactionListItemProps): React.ReactElement => {
  const styles = useStyleSheet(themedStyles);
  const transaction = props.transactionItem;

  const renderTransactionValue = (itemProps: DataElement<TransactionAttributes>) => {
    let tAttributes = itemProps.attributes.transactions[0]
    const value = `${tAttributes.currency_symbol}${parseFloat(tAttributes.amount).toFixed(tAttributes.currency_decimal_places)}`;
    switch (tAttributes.type) {
      case TRANSACTION_TYPE_DEPOSIT:
        return (viewProps: ViewProps) => (
          <>
            <Icon
              fill={styles.transactionValueDeposit.color}
              style={{ ...styles.transactionArrowIcon }}
              name="diagonal-arrow-right-up"
            />
            <Text
              style={{
                ...styles.transactionValue,
                ...styles.transactionValueDeposit,
              }}>
              {value}
            </Text>
          </>
        );
      case TRANSACTION_TYPE_WITHDRAWAL:
        return (viewProps: ViewProps) => (
          <>
            <Icon
              fill={styles.transactionValueWithdrawal.color}
              style={styles.transactionArrowIcon}
              name="diagonal-arrow-right-down"
            />
            <Text
              style={{
                ...styles.transactionValue,
                ...styles.transactionValueWithdrawal,
              }}>
              {value}
            </Text>
          </>
        );
      case TRANSACTION_TYPE_TRANSFER:
        return (viewProps: ViewProps) => (
          <>
            <Icon
              fill={styles.transactionValueTransfer.color}
              style={styles.transactionArrowIcon}
              name="arrow-forward"
            />
            <Text
              style={{
                ...styles.transactionValue,
                ...styles.transactionValueTransfer,
              }}>
              {value}
            </Text>
          </>
        );
    }
  };

  return (
    <>
      <ListItem
        style={styles.transactionItem}
        title={transaction.attributes.transactions[0].description}
        description={
          `${moment(transaction.attributes.created_at).fromNow()} • ${moment(transaction.attributes.created_at).format('DD MMM YYYY H:mm')}`
        }
        accessoryRight={renderTransactionValue({ ...transaction })}
      />
      {(props.showDivider !== undefined ? props.showDivider : true) && <Divider></Divider>}
    </>
  );
};

export default TransactionListItem;

const themedStyles = StyleService.create({
  transactionItem: {
    padding: 0,
    margin: 0,
    paddingHorizontal: 16,
  },
  transactionValue: {
    fontSize: 14,
  },
  transactionArrowIcon: {
    width: 16,
    height: 16,
  },
  transactionValueDeposit: {
    color: 'color-success-500',
  },
  transactionValueWithdrawal: {
    color: 'color-danger-500',
  },
  transactionValueTransfer: {
    color: 'color-warning-600',
  },
});
