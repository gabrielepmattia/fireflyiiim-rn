import { components } from "@eva-design/eva/mapping";
import React, { useRef, useState } from "react";
import { TextInput, View } from "react-native";
import { Button, StyleService, Text, useStyleSheet } from "@ui-kitten/components";
import { ModalFunctionalProps } from "../../../../services/modal.component";

/*
 * Interfaces
 */

export interface ModalPanelMessagePNProps extends ModalFunctionalProps {
  title?: string | null
  message?: string | null

  dismissClb?: () => void
  okClb?: () => void
}

/*
 * Component
 */

const ModalPanelMessagePNComponent = (props: ModalPanelMessagePNProps) => {

  const styles = useStyleSheet(themedStyles);

  React.useEffect(() => {
  }, [])

  /*
   * Listeners
   */

  const save = () => {
    if (props.okClb) props.okClb();
    if (props.close != undefined) props.close()
  }

  const dismiss = () => {
    if (props.dismissClb) props.dismissClb();
    if (props.close != undefined) props.close()
  }

  return (
    <View>
      <Text category="h2" style={{ paddingBottom: 16 }}>{props.title ? props.title : ""}</Text>
      <Text category="p1" style={{ paddingBottom: 16 }}>{props.message ? props.message : ""}</Text>

      <View style={styles.buttonRow}>
        <Button style={styles.button} onPress={() => dismiss()} appearance='ghost'>
          DISMISS
        </Button>
        <Button style={styles.button} onPress={() => save()} appearance='ghost'>
          OK
        </Button>
      </View>
    </View>
  )

}

/*
 * Export
 */

export default ModalPanelMessagePNComponent;

/*
 * Styles
 */

const themedStyles = StyleService.create({
  buttonRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: "flex-end"
  }, button: {}
});
