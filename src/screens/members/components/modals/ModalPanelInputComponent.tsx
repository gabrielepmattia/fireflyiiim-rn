import { components } from "@eva-design/eva/mapping";
import React, { useRef, useState } from "react";
import { TextInput, View } from "react-native";
import { Button, StyleService, Text, useStyleSheet } from "@ui-kitten/components";
import { ModalFunctionalProps } from "../../../../services/modal.component";

/*
 * Interfaces
 */

export interface ModalPanelInputProps extends ModalFunctionalProps {
  title?: string | null
  inputPlaceholder?: string | null
  value?: string

  dismissTextClb?: (input: string | null) => void
}

/*
 * Component
 */

const ModalPanelInputComponent = (props: ModalPanelInputProps) => {

  const styles = useStyleSheet(themedStyles);

  const [inputContent, setInputContent] = useState("")

  const editTextInputRef = useRef<TextInput>(null)

  React.useEffect(() => {
    if (props.value) setInputContent(props.value)
  }, [props.visibility])

  /*
   * Listeners
   */

  const save = () => {
    if (props.dismissTextClb) props.dismissTextClb(inputContent);
    if (props.close != undefined) props.close()
  }

  const dismiss = () => {
    if (props.dismissTextClb) props.dismissTextClb(null);
    if (props.close != undefined) props.close()
  }

  return (
    <View>
      <Text category="h2">{props.title ? props.title : ""}</Text>
      <TextInput
        style={{ fontSize: 16 }}
        autoFocus={true}
        multiline={true}
        value={inputContent}
        onChangeText={(text: string) => setInputContent(text)}
        placeholder="Input text here"/>
      <View style={styles.buttonRow}>
        <Button style={styles.button} onPress={() => dismiss()} appearance='ghost'>
          DISMISS
        </Button>
        <Button style={styles.button} onPress={() => save()} appearance='ghost'>
          OK
        </Button>
      </View>
    </View>
  )

}

/*
 * Export
 */

export default ModalPanelInputComponent;

/*
 * Styles
 */

const themedStyles = StyleService.create({
  buttonRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: "flex-end"
  }, button: {}
});
