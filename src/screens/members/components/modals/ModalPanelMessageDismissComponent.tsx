import { components } from "@eva-design/eva/mapping";
import React, { useRef, useState } from "react";
import { TextInput, View } from "react-native";
import { Button, StyleService, Text, useStyleSheet } from "@ui-kitten/components";
import { ModalFunctionalProps } from "../../../../services/modal.component";

/*
 * Interfaces
 */

export interface ModalPanelMessageDismissProps extends ModalFunctionalProps {
  title?: string | null
  message?: string | null

  dismissClb?: () => void
}

/*
 * Component
 */

const ModalPanelMessageDismissComponent = (props: ModalPanelMessageDismissProps) => {

  const styles = useStyleSheet(themedStyles);

  React.useEffect(() => {
  }, [])

  /*
   * Listeners
   */

  const dismiss = () => {
    if (props.dismissClb) props.dismissClb();
    if (props.close != undefined) props.close()
  }

  return (
    <View>
      <Text category="h2" style={{ paddingBottom: 16 }}>{props.title ? props.title : ""}</Text>
      <Text category="p1" style={{ paddingBottom: 16 }}>{props.message ? props.message : ""}</Text>

      <View style={styles.buttonRow}>
        <Button style={styles.button} onPress={() => dismiss()} appearance='ghost'>
          DISMISS
        </Button>
      </View>
    </View>
  )

}

/*
 * Export
 */

export default ModalPanelMessageDismissComponent;

/*
 * Styles
 */

const themedStyles = StyleService.create({
  buttonRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: "flex-end"
  }, button: {}
});
