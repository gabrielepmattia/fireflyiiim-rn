import { List, StyleService, useStyleSheet } from "@ui-kitten/components";
import React, { useState } from 'react';
import { StyleProp, ViewStyle } from "react-native";
import { Summary, SummaryItem } from "lib/fireflyiii-api/models/summary";
import InfoCard from "./InfoCard";


/*
 * Interfaces
 */

export interface Props {
  summary?: Summary
  style?: StyleProp<ViewStyle>
}

interface InfoBox {
  title: string;
  value: string;
  subtitle: string;
}
/*
 * Component
 */

const HorizontalSummaryCards = (props: Props) => {

  const [infoBoxes, setInfoBoxes] = useState(new Array<InfoBox>())

  React.useEffect(() => {
    if (props.summary == undefined) return;

    let infoBoxes = new Array<InfoBox>();

    for (let key in props.summary) {
      if (!props.summary.hasOwnProperty(key)) continue;
      let summaryItem = props.summary[key] as SummaryItem;
      infoBoxes.push({
        title: summaryItem.title,
        value: summaryItem.value_parsed,
        subtitle: summaryItem.sub_title,
      });
    }
    setInfoBoxes(infoBoxes);

  }, [props.summary])

  const styles = useStyleSheet(themedStyles);

  /*
   * Renders
   */

  const renderHorizontalCardsItem = (item: any) => {
    const infoBox = item.item as InfoBox;
    return <InfoCard {...infoBox} />;
  };

  return (
    <List
      style={styles.horizontalCardsList}
      horizontal={true}
      data={infoBoxes}
      renderItem={renderHorizontalCardsItem}
    />
  )

}

/*
 * Export
 */

export default HorizontalSummaryCards;

/*
 * Styles
 */

const themedStyles = StyleService.create({
  horizontalCardsList: {
    // backgroundColor: "background-basic-color-1"
    // display: 'flex',
    // flexDirection: 'row',
    // padding: 0,
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 16,
    paddingRight: 16,
  }
});
