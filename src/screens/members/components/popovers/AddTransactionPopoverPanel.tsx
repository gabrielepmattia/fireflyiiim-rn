import {
  Button,
  ButtonGroup,
  Icon,
  StyleService,
  Tab,
  TabBar,
  Text,
  useStyleSheet,
  useTheme
} from "@ui-kitten/components";
// @ts-ignore
import { accounting } from "accounting";
import moment from "moment";
import React, { Dispatch, SetStateAction, useRef, useState } from "react";
import { Animated, Dimensions, StyleProp, View, ViewStyle } from "react-native";
import { TouchableOpacity, TouchableWithoutFeedback } from "react-native-gesture-handler";
import { FlatGrid } from "react-native-super-grid";
import { connect } from "react-redux";
import { FireflyIIIApi } from "../../../../../lib/fireflyiii-api/api";
import {
  ACCOUNT_ROLE_ASSET_DEFAULT,
  ACCOUNT_TYPE_CASH,
  renderAccountTypeString
} from "../../../../../lib/fireflyiii-api/models/account";
import { API_DATE_FORMAT } from "../../../../../lib/fireflyiii-api/models/api";
import {
  Transaction, TRANSACTION_TYPE_DEPOSIT, TRANSACTION_TYPE_TRANSFER, TRANSACTION_TYPE_WITHDRAWAL,
  TransactionAttributes,
  TransactionStoreElement
} from "../../../../../lib/fireflyiii-api/models/transaction";
import ApiService from "../../../../services/api.service";
import ModalService from "../../../../services/modal.service";
import { CacheState } from "../../../../store/cache/types";
import { RootState } from "../../../../store/store";
import { refreshDashboardTransactions } from "../../../../store/utils";
import { getDefaultCategories } from "../../../../utils/categories";
import { getColor } from "../../../../utils/colors";
import { ArrowDownwardOutline, ArrowUpwardOutline, MenuArrowOutline } from "../../../../utils/icons";
import ModalPanelInputComponent from "../modals/ModalPanelInputComponent";
import ModalPanelMessageDismissComponent from "../modals/ModalPanelMessageDismissComponent";
import ModalPanelMessagePNComponent from "../modals/ModalPanelMessagePNComponent";

/*
 * Interfaces
 */

interface Category {
  name: string
  id?: string
}

interface AddTransactionPopoverPanelProps {
  visibility: boolean
  close: () => void

  // mapped
  cache: CacheState
}

interface AddTransactionPopoverPanelSceneProps extends AddTransactionPopoverPanelProps {
  goToScene: (sceneType: SceneTypes) => void
  transactionMeta: TransactionMetadata
  setTransactionMeta: Dispatch<SetStateAction<TransactionMetadata>>
}

interface KeypadButtonProps {
  text?: string
  innerComponent?: React.ReactElement
  action: () => void
  actionKey?: boolean
  doubleVertical?: boolean
  rows?: number
  cols?: number
  i?: number
  j?: number
}

interface TransactionMetadata {
  fromAccountName: string
  fromAccountID: string
  fromAccountType: string
  fromAccountRole: string
  fromAccountCurrencySymbol: string
  fromAccountCurrencyCode: string
  toName: string
  toID: string
  amount: number
  transactionType: TransactionType
  categoryName: string
  categoryID?: string
  description: string
  date: Date
}

/*
 * Scenes
 */

interface Scene {
  type: number,
  height: number,
}

enum SceneTypes {
  SELECT_CATEGORY,
  ADD_DATA
}

const sceneSelectCategoryTopPanelHeight = 110
const sceneSelectCategoryBottomPanelHeight = 300
const sceneSelectCategoryTotalPanelHeight = sceneSelectCategoryTopPanelHeight + sceneSelectCategoryBottomPanelHeight
const sceneAddDataTotalPanelHeight = 495

/*
 * Const
 */

enum TransactionType {
  Revenue, Expense, Transfer
}

const OPERATOR_DIVIDE = "÷"
const OPERATOR_TIMES = "×"
const OPERATOR_PLUS = "+"
const OPERATOR_MINUS = "-"

const SCENES: Array<Scene> = [
  { type: SceneTypes.SELECT_CATEGORY, height: sceneSelectCategoryTotalPanelHeight },
  { type: SceneTypes.ADD_DATA, height: sceneAddDataTotalPanelHeight },
]

/*
 * Sub-components
 */

const AddTransactionSceneSelectCategoryComponent: React.FC<AddTransactionPopoverPanelSceneProps> = (props: AddTransactionPopoverPanelSceneProps) => {

  const styles = useStyleSheet(sceneSelectCategoryThemedStyles);
  const [selectedAccount, setSelectedAccount] = useState(0)

  const setFromAccount = (accountIndex: number) => {
    setSelectedAccount(accountIndex)

    let transactionType = props.transactionMeta.transactionType
    if (props.cache.accounts[accountIndex].attributes.type === ACCOUNT_TYPE_CASH && props.transactionMeta.transactionType === TransactionType.Expense)
      transactionType = TransactionType.Revenue

    props.setTransactionMeta({
      ...props.transactionMeta,
      fromAccountName: props.cache.accounts[accountIndex].attributes.name,
      fromAccountRole: props.cache.accounts[accountIndex].attributes.account_role,
      fromAccountType: props.cache.accounts[accountIndex].attributes.type,
      fromAccountID: props.cache.accounts[accountIndex].id,
      fromAccountCurrencySymbol: props.cache.accounts[accountIndex].attributes.currency_symbol,
      fromAccountCurrencyCode: props.cache.accounts[accountIndex].attributes.currency_code,
      transactionType: transactionType
    })
  }

  const setCategory = (category: Category) => {
    // Set the state
    props.setTransactionMeta({
      ...props.transactionMeta,
      categoryName: category.name,
      categoryID: category.id,
    })
    // Change scene to add data
    props.goToScene(SceneTypes.ADD_DATA)
  }

  const setTransactionType = (transactionType: TransactionType) => {
    if (props.transactionMeta.fromAccountType === ACCOUNT_TYPE_CASH && transactionType === TransactionType.Expense) return
    props.setTransactionMeta({ ...props.transactionMeta, transactionType })
  }

  /*
   * Utils
   */

  const generateCategoriesList = (): Array<Category> => {
    let output: Array<Category> = []
    let userCategories = props.cache.categories
    let defaultCategories = getDefaultCategories()
    userCategories.forEach((category) => {
      output.push({ name: category.attributes.name, id: category.id })
    })
    defaultCategories.forEach((category) => {
      output.push({ name: category })
    })
    return output
  }

  const getAccountBalanceFormatted = (type: string, balance: number, currency: string) => {
    if (type === ACCOUNT_TYPE_CASH) return "-"
    return accounting.formatMoney(balance, `${currency} `)
  }

  return (
    <View>
      <View style={styles.topPanel}>
        <View style={styles.topPanelTop}>
          <View style={{ flex: 6 }}>
            <Text style={{ color: "#ffffff" }} category="h4">
              {props.transactionMeta.fromAccountName}
            </Text>
            <Text style={{ color: "#ffffff" }} category="c1">
              {renderAccountTypeString(props.transactionMeta.fromAccountType)}
            </Text>
          </View>
          <View style={{ flex: 1, alignItems: "flex-end" }}>
            <TouchableOpacity><Icon style={{ width: 28, height: 28 }} fill='#ffffff'
                                    name={props.transactionMeta.fromAccountRole === ACCOUNT_ROLE_ASSET_DEFAULT ? 'star' : 'star-outline'}/></TouchableOpacity>
          </View>
        </View>
        <View style={styles.topPanelBottom}>
          <View style={{ flex: 1 }}>
            {selectedAccount > 0 &&
            <TouchableOpacity onPress={() => {
              if (selectedAccount > 0) setFromAccount(selectedAccount - 1)
            }}>
                <Icon style={{ width: 20, height: 20 }} fill='#ffffff' name='arrow-ios-back-outline'/>
            </TouchableOpacity>}
          </View>
          <View style={{ flex: 1, alignItems: "center" }}>
            <Text style={{ color: "#ffffff" }} category="c1">Current Balance</Text>
            <Text style={{ color: "#ffffff" }} category="p1">
              {getAccountBalanceFormatted(props.cache.accounts[selectedAccount].attributes.type, props.cache.accounts[selectedAccount].attributes.current_balance, props.cache.accounts[selectedAccount].attributes.currency_symbol)}
            </Text>
          </View>
          <View style={{ flex: 1, alignItems: "flex-end" }}>
            {selectedAccount < props.cache.accounts.length - 1 &&
            <TouchableOpacity onPress={() => {
              if (selectedAccount < props.cache.accounts.length - 1) setFromAccount(selectedAccount + 1)
            }}>
                <Icon style={{ width: 20, height: 20 }} fill='#ffffff' name='arrow-ios-forward-outline'/>
            </TouchableOpacity>}
          </View>
        </View>
      </View>
      <View style={styles.bottomPanel}>
        <View style={{ alignItems: "center", marginVertical: 10 }}>
          <ButtonGroup style={{}} size="large" status='primary' appearance="outline">
            <Button onPress={() => setTransactionType(TransactionType.Expense)}
                    disabled={props.transactionMeta.fromAccountType === ACCOUNT_TYPE_CASH}
                    style={props.transactionMeta.transactionType === TransactionType.Expense ? styles.buttonTransactionTypeSelected : {}}>Expense</Button>
            <Button onPress={() => setTransactionType(TransactionType.Revenue)}
                    style={props.transactionMeta.transactionType === TransactionType.Revenue ? styles.buttonTransactionTypeSelected : {}}>Revenue</Button>
            <Button onPress={() => setTransactionType(TransactionType.Transfer)}
                    style={props.transactionMeta.transactionType === TransactionType.Transfer ? styles.buttonTransactionTypeSelected : {}}>Transfer</Button>
          </ButtonGroup>
        </View>
        <FlatGrid
          itemDimension={80}
          data={generateCategoriesList()}
          style={styles.gridView}
          // staticDimension={300}
          // fixed
          spacing={10}
          renderItem={({ item, index }) => (
            <TouchableOpacity onPress={() => setCategory(item)}>
              <View style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
                <Text style={{ paddingBottom: 6, textTransform: "capitalize" }} category="c1">{item.name}</Text>
                <View style={[styles.itemContainer, { backgroundColor: getColor(index) }]}>
                  <Icon style={{ height: 40, width: 40 }} name='question-mark-outline' fill="#ffffff"/>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    </View>
  )
}

const AddTransactionSceneAddDataComponent: React.FC<AddTransactionPopoverPanelSceneProps> = (props: AddTransactionPopoverPanelSceneProps) => {
  const MODULE = "AddTransactionSceneAddDataComponent"

  enum Keys {
    KEY_0, KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8, KEY_9,
    KEY_CUR, KEY_DOT, KEY_EQUAL, KEY_DEL, KEY_CAL, DIVIDE, TIMES, PLUS, MINUS
  }

  const [currencySymbol, setCurrencySymbol] = useState('€')
  const [calcDisplay, setCalcDisplay] = useState(`${currencySymbol} 0.00`)
  const [calcDisplayClear, setCalcDisplayClear] = useState(true)
  const [calcValue, setCalcValue] = useState(0.0)
  const [inExpression, setInExpression] = useState(false)

  const styles = useStyleSheet(sceneAddDataThemedStyles)
  const theme = useTheme()

  React.useEffect(() => {
    setCurrencySymbol(props.transactionMeta.fromAccountCurrencySymbol)
  }, [props.transactionMeta])

  /**
   * Update the display, if function should be executed then returns true, otherwise false
   * @param key
   */
  const keyUpdateDisplay = (key: Keys): boolean => {

    const inputNumber = (n: number) => {
      console.debug(`[${MODULE}] inputNumber(${n}) = calcDisplayClear:${calcDisplayClear}`)
      if (calcDisplayClear) {
        setCalcDisplay(`${currencySymbol} ${n}`)
        setCalcDisplayClear(false)
      } else setCalcDisplay(`${calcDisplay}${n}`)
      // update the value if not in expression
      if (!inExpression) {
        let amount = parseFloat(`${calcDisplay}${n}`.split(" ")[1])
        setCalcValue(amount)
        updateTransactionAmount(amount)
      }
    }

    const inputOperator = (o: string) => {
      if (calcDisplayClear) return
      if (inExpression) {
        setCalcDisplay(`${accounting.formatMoney(evalDisplayExpression(), `${currencySymbol} `, "2")} ${o} `)
        return
      }
      setInExpression(true)
      setCalcDisplay(`${calcDisplay} ${o} `)
    }

    let currentCalcValue = calcValue
    switch (key) {
      case Keys.KEY_0:
        inputNumber(0)
        break
      case Keys.KEY_1:
        inputNumber(1)
        break
      case Keys.KEY_2:
        inputNumber(2)
        break
      case Keys.KEY_3:
        inputNumber(3)
        break
      case Keys.KEY_4:
        inputNumber(4)
        break
      case Keys.KEY_5:
        inputNumber(5)
        break
      case Keys.KEY_6:
        inputNumber(6)
        break
      case Keys.KEY_7:
        inputNumber(7)
        break
      case Keys.KEY_8:
        inputNumber(8)
        break
      case Keys.KEY_9:
        inputNumber(9)
        break
      case Keys.KEY_DEL:
        setInExpression(false)
        setCalcValue(0.00)
        setCalcDisplay(`${currencySymbol} 0.00`)
        setCalcDisplayClear(true)
        updateTransactionAmount(0.0)
        break
      case Keys.KEY_CAL:
        return true
      case Keys.KEY_CUR:
        return true
      case Keys.KEY_DOT:
        if (!calcDisplayClear) setCalcDisplay(`${calcDisplay}.`)
        break
      case Keys.DIVIDE:
        inputOperator("÷")
        break
      case Keys.TIMES:
        inputOperator("×")
        break
      case Keys.PLUS:
        inputOperator("+")
        break
      case Keys.MINUS:
        inputOperator("-")
        break
      case Keys.KEY_EQUAL:
        if (inExpression) {
          evalDisplayExpression()
          setCalcDisplay(`${accounting.formatMoney(evalDisplayExpression(), `${currencySymbol} `, "2")}`)
          setInExpression(false)
          return false
        }
        return true
    }
    return true
  }

  /**
   * Execute a function for the key
   * @param key
   */
  const keyExecuteFunction = (key: Keys) => {
    switch (key) {
      case Keys.KEY_EQUAL:
        console.log(`[${MODULE}] calcValue ${calcValue}`)
        // save the amount
        props.setTransactionMeta({ ...props.transactionMeta, amount: calcValue })
        // check fields
        console.log(`[${MODULE}] tr ${JSON.stringify(props.transactionMeta)}`)
        if (!validateFields()) {
          showMessageDismissModal(
            "Add Transaction Error",
            "Please check if: description is present, amount is greater than zero and it is not an expression")
        } else {
          showMessageModal(
            "Add Transaction",
            "Are you sure to add the transaction?",
            () => {
              apiSaveTransaction()
            })
        }
    }
  }

  /**
   * Handle the pressing of a single button
   * @param key
   */
  const press = (key: Keys): (() => void) => {
    return () => {
      if (keyUpdateDisplay(key)) keyExecuteFunction(key)
    }
  }

  /*
   * Core
   */

  /**
   * Evaluate the expression displayed and replace with result
   */
  const evalDisplayExpression = (): number => {
    let displayComponents = calcDisplay.split(" ")
    console.debug(`[${MODULE}] evalDisplayExpression component: ${displayComponents}`)
    if (displayComponents.length != 4) keyUpdateDisplay(Keys.KEY_DEL)
    let firstNumber = parseFloat(displayComponents[1])
    let operator = displayComponents[2]
    let secondNumber = parseFloat(displayComponents[3])

    // perform the operation
    let result = 0.0
    switch (operator) {
      case OPERATOR_DIVIDE:
        if (secondNumber !== 0.0) result = firstNumber / secondNumber
        break
      case OPERATOR_MINUS:
        result = firstNumber - secondNumber
        break
      case OPERATOR_PLUS:
        result = firstNumber + secondNumber
        break
      case OPERATOR_TIMES:
        result = firstNumber * secondNumber
        break
    }
    setCalcValue(result)
    // setCalcDisplay(accounting.formatMoney(currencySymbol, result))
    // setInExpression(false)
    return result
  }

  /*
   * UI
   */

  const getKeyText = (key: Keys): string => {
    switch (key) {
      case Keys.KEY_0:
        return "0"
      case Keys.KEY_1:
        return "1"
      case Keys.KEY_2:
        return "2"
      case Keys.KEY_3:
        return "3"
      case Keys.KEY_4:
        return "4"
      case Keys.KEY_5:
        return "5"
      case Keys.KEY_6:
        return "6"
      case Keys.KEY_7:
        return "7"
      case Keys.KEY_8:
        return "8"
      case Keys.KEY_9:
        return "9"
      case Keys.KEY_DEL:
        return "DEL"
      case Keys.KEY_CAL:
        return "CAL"
      case Keys.KEY_CUR:
        return "€"
      case Keys.KEY_DOT:
        return "."
      case Keys.DIVIDE:
        return "÷"
      case Keys.TIMES:
        return "×"
      case Keys.PLUS:
        return "+"
      case Keys.MINUS:
        return "-"
      case Keys.KEY_EQUAL:
        return "="
      default:
        return ""
    }
  }

  const getKeyInnerComponent = (key: Keys): React.ReactElement | undefined => {
    switch (key) {
      case Keys.KEY_DEL:
        return (<Icon style={{ height: 26, width: 26 }} name='backspace' fill="#00000090"/>)
      case Keys.KEY_CAL:
        return (<Icon style={{ height: 26, width: 26 }} name='calendar' fill="#00000090"/>)
      case Keys.KEY_EQUAL:
        return inExpression ? (<Text style={styles.keypadButtonText}>=</Text>) : (
          <Icon style={{ height: 26, width: 26 }} name='checkmark' fill="#00000090"/>)
    }
  }

  const getToTitleString = () => {
    if (props.transactionMeta.toName === undefined || props.transactionMeta.toName === '') return "category"
    return "account"
  }

  const getToNameString = () => {
    if (props.transactionMeta.toName === undefined || props.transactionMeta.toName === '') return props.transactionMeta.categoryName
    return props.transactionMeta.toName
  }

  const getAmountStatus = () => {
    switch (props.transactionMeta.transactionType) {
      case TransactionType.Expense:
        return "danger"
      case TransactionType.Revenue:
        return "success"
      case TransactionType.Transfer:
        return "warning"
    }
  }

  const getTransactionTypeName = () => {
    switch (props.transactionMeta.transactionType) {
      case TransactionType.Expense:
        return "Expense"
      case TransactionType.Revenue:
        return "Revenue"
      case TransactionType.Transfer:
        return "Transfer"
    }
  }

  const getTransactionDescription = (): string => {
    if (!getTransactionHasDescription()) return "Provide a description here"
    return props.transactionMeta.description
  }

  const getTransactionHasDescription = (): boolean => {
    return !(props.transactionMeta.description == undefined || props.transactionMeta.description == '')
  }

  /*
   * Utils
   */

  const showInputModal = () => {
    // @ts-ignore
    ModalService.getInstance().setModalPanelProps({
      title: "Add description",
      inputPlaceholder: "Input a transaction description here",
      dismissTextClb: input => {
        if (input == null) return;
        props.setTransactionMeta({
          ...props.transactionMeta,
          description: input
        })
      },
      value: props.transactionMeta.description
    })
    ModalService.getInstance().setModalPanel(ModalPanelInputComponent)
    ModalService.getInstance().show(true)
  }

  const showMessageModal = (title: string, message: string, okClb?: () => void) => {
    // @ts-ignore
    ModalService.getInstance().setModalPanelProps({
      title,
      message,
      okClb,
      dismissClb: () => {
      }
    })
    ModalService.getInstance().setModalPanel(ModalPanelMessagePNComponent)
    ModalService.getInstance().show(true)
  }

  const showMessageDismissModal = (title: string, message: string, dismissClb?: () => void) => {
    // @ts-ignore
    ModalService.getInstance().setModalPanelProps({
      title,
      message,
      dismissClb
    })
    ModalService.getInstance().setModalPanel(ModalPanelMessageDismissComponent)
    ModalService.getInstance().show(true)
  }

  /**
   * Save the transaction to server
   */
  const apiSaveTransaction = () => {
    let transaction = {
      description: props.transactionMeta.description,
      amount: props.transactionMeta.amount,
      date: moment(props.transactionMeta.date).format(API_DATE_FORMAT),
      source_id: props.transactionMeta.fromAccountID,
      destination_id: props.transactionMeta.toID,
      category_id: props.transactionMeta.categoryID,
      category_name: props.transactionMeta.categoryName,
      type: getTransactionApiType(props.transactionMeta.transactionType)
    } as Transaction
    let transactionStoreElement = {
      apply_rules: false,
      group_title: "",
      transactions: [transaction]
    } as TransactionStoreElement

    console.debug(`[${MODULE}] apiSaveTransaction: called with ${JSON.stringify(transactionStoreElement)}`)

    ApiService.getInstance().handler().addTransaction(transactionStoreElement).then(res => {
      console.debug(`[${MODULE}] apiSaveTransaction: res=${res.status}`)
      if (props.close && res.ok) props.close()
      if (!res.ok) showMessageDismissModal("Add Transaction Error", "There was an error adding the transaction")
    }).catch(err => {
      showMessageDismissModal("Add Transaction Error", "There was an error adding the transaction")
      console.error(`[${MODULE}] apiSaveTransaction: Cannot add transaction: ${err}`)
    })
  }

  /**
   * Retrieve the transaction type for the api call
   * @param type
   */
  const getTransactionApiType = (type: TransactionType) => {
    switch (type) {
      case TransactionType.Transfer:
        return TRANSACTION_TYPE_TRANSFER
      case TransactionType.Expense:
        return TRANSACTION_TYPE_WITHDRAWAL
      case TransactionType.Revenue:
        return TRANSACTION_TYPE_DEPOSIT
    }
  }

  const validateFields = () => {
    if (inExpression) return false
    if (props.transactionMeta.description === "" || props.transactionMeta.description === undefined) return false
    if (props.transactionMeta.amount === 0.0 || props.transactionMeta.amount === undefined) return false
    return true
  }

  const updateTransactionAmount = (amount: number) => {
    props.setTransactionMeta({ ...props.transactionMeta, amount })
  }

  return (
    <View style={styles.panel}>
      <View style={styles.directionsPanel}>
        <View
          style={[styles.directionPanelItem, styles.directionPanelItemLeft, { backgroundColor: theme["color-primary-500"] }]}>
          <TouchableOpacity>
            <View><Text style={{ color: "#ffffff", paddingBottom: 4 }} category="c1">From account</Text></View>
            <View><Text style={{ color: "#ffffff" }} category="h5">{props.transactionMeta.fromAccountName}</Text></View>
          </TouchableOpacity>
        </View>
        <View style={[styles.directionPanelItem, styles.directionPanelItemRight, { backgroundColor: getColor(2) }]}>
          <TouchableOpacity>
            <View><Text style={{ color: "#ffffff", paddingBottom: 4 }}
                        category="c1">{`To ${getToTitleString()}`}</Text></View>
            <View><Text style={{ color: "#ffffff", textTransform: "capitalize" }}
                        category="h5">{getToNameString()}</Text></View>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.amountPanel}>
        <Text status={getAmountStatus()} style={{ textAlign: "center", paddingBottom: 4 }}
              category="c1">{getTransactionTypeName()}</Text>
        <Text status={getAmountStatus()} style={{ textAlign: "center", fontSize: 24 }}>{calcDisplay}</Text>
      </View>
      <View style={styles.descriptionPanel}>
        <TouchableOpacity onPress={showInputModal}>
          <Text
            numberOfLines={1}
            style={{
              textAlign: "center",
              fontStyle: getTransactionHasDescription() ? "normal" : "italic",
              color: "#00000050"
            }}
            category="p1">{getTransactionDescription()}</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.keypadPanel}>
        <View style={styles.keypadRow}>
          <KeypadButton action={press(Keys.DIVIDE)} text={getKeyText(Keys.DIVIDE)} actionKey={true}/>
          <KeypadButton action={press(Keys.KEY_7)} text={getKeyText(Keys.KEY_7)}/>
          <KeypadButton action={press(Keys.KEY_8)} text={getKeyText(Keys.KEY_8)}/>
          <KeypadButton action={press(Keys.KEY_9)} text={getKeyText(Keys.KEY_9)}/>
          <KeypadButton action={press(Keys.KEY_DEL)} innerComponent={getKeyInnerComponent(Keys.KEY_DEL)}
                        actionKey={true}/>
        </View>
        <View style={styles.keypadRow}>
          <KeypadButton action={press(Keys.TIMES)} text={getKeyText(Keys.TIMES)} actionKey={true}/>
          <KeypadButton action={press(Keys.KEY_4)} text={getKeyText(Keys.KEY_4)}/>
          <KeypadButton action={press(Keys.KEY_5)} text={getKeyText(Keys.KEY_5)}/>
          <KeypadButton action={press(Keys.KEY_6)} text={getKeyText(Keys.KEY_6)}/>
          <KeypadButton action={press(Keys.KEY_CAL)} innerComponent={getKeyInnerComponent(Keys.KEY_CAL)}
                        actionKey={true}/>
        </View>
        <View style={styles.keypadRow}>
          <View style={{ flexDirection: "column" }}>
            <View style={styles.keypadRow}>
              <KeypadButton action={press(Keys.PLUS)} text={getKeyText(Keys.PLUS)} actionKey={true}/>
              <KeypadButton action={press(Keys.KEY_1)} text={getKeyText(Keys.KEY_1)}/>
              <KeypadButton action={press(Keys.KEY_2)} text={getKeyText(Keys.KEY_2)}/>
              <KeypadButton action={press(Keys.KEY_3)} text={getKeyText(Keys.KEY_3)}/>
            </View>
            <View style={styles.keypadRow}>
              <KeypadButton action={press(Keys.MINUS)} text={getKeyText(Keys.MINUS)} actionKey={true}/>
              <KeypadButton action={press(Keys.KEY_CUR)} text={getKeyText(Keys.KEY_CUR)}/>
              <KeypadButton action={press(Keys.KEY_0)} text={getKeyText(Keys.KEY_0)}/>
              <KeypadButton action={press(Keys.KEY_DOT)} text={getKeyText(Keys.KEY_DOT)}/>
            </View>
          </View>
          <KeypadButton action={press(Keys.KEY_EQUAL)}
                        innerComponent={getKeyInnerComponent(Keys.KEY_EQUAL)}
                        doubleVertical={true}
                        actionKey={true}/>
        </View>
      </View>
      <View style={styles.datePanel}>
        <Text style={styles.datePanelText} category="c1">TODAY, 22th May 2020</Text>
      </View>
    </View>
  )
}

const KeypadButton: React.FC<KeypadButtonProps> = (props: KeypadButtonProps) => {
  const styles = useStyleSheet(keypadStyles)

  return (
    <TouchableOpacity onPress={props.action}>
      <View
        style={[
          props.doubleVertical ? styles.keypadVerticalDoubleButton : styles.keypadButton,
          props.actionKey ? styles.keypadFunctionButton : {}]}>
        {props.text != null && <Text style={styles.keypadButtonText}>{props.text}</Text>}
        {props.innerComponent != null && props.innerComponent}
      </View>
    </TouchableOpacity>
  )
}

/*
 * Main Component
 */

const AddTransactionPopoverPanelComponent: React.FC<AddTransactionPopoverPanelProps> = (props: AddTransactionPopoverPanelProps) => {

  const [scene, setScene] = useState(SceneTypes.SELECT_CATEGORY)
  const [visibility, setVisibility] = useState(false)
  const [inTransition, setInTransition] = useState(false)

  // shared state
  const [transactionMeta, setTransactionMeta] = useState({} as TransactionMetadata)

  const animatedSlidingCardSelectCategorySceneY = useRef(new Animated.Value(SCENES[SceneTypes.SELECT_CATEGORY].height)).current;
  const animatedSlidingCardAddDataSceneY = useRef(new Animated.Value(SCENES[SceneTypes.ADD_DATA].height)).current;

  const styles = useStyleSheet(generalStyles)

  React.useEffect(() => {
    if (visibility == props.visibility) return;
    if (visibility) {
      disappear(() => setVisibility(props.visibility));
    } else {
      // init transaction meta
      setTransactionMeta({
        fromAccountName: props.cache.accounts[0].attributes.name,
        fromAccountRole: props.cache.accounts[0].attributes.account_role,
        fromAccountID: props.cache.accounts[0].id,
        fromAccountType: props.cache.accounts[0].attributes.type,
        fromAccountCurrencySymbol: props.cache.accounts[0].attributes.currency_symbol,
        fromAccountCurrencyCode: props.cache.accounts[0].attributes.currency_code,
        transactionType: TransactionType.Revenue,
        toName: "",
        toID: "",
        date: new Date()
      } as TransactionMetadata)
      setVisibility(props.visibility);
      appear();
    }
  }, [props.visibility]);

  /*
   * Animations
   */

  const appear = (clb?: Animated.EndCallback) => {
    setScene(SceneTypes.SELECT_CATEGORY)
    appearSelectCategory(clb)
  }

  const disappear = (clb?: Animated.EndCallback) => {
    switch (scene) {
      case SceneTypes.ADD_DATA:
        disappearAddData(clb)
        break
      case SceneTypes.SELECT_CATEGORY:
        disappearSelectCategory(clb)
        break
    }
  }

  const appearSelectCategory = (clb?: Animated.EndCallback) => {
    Animated.spring(animatedSlidingCardSelectCategorySceneY, {
      toValue: 0,
      duration: 250,
      useNativeDriver: true,
      bounciness: 0,
      restDisplacementThreshold: 40,
      restSpeedThreshold: 100,
    } as Animated.SpringAnimationConfig).start(clb);
  };

  const disappearSelectCategory = (clb?: Animated.EndCallback) => {
    Animated.spring(animatedSlidingCardSelectCategorySceneY, {
      toValue: SCENES[SceneTypes.SELECT_CATEGORY].height,
      duration: 250,
      useNativeDriver: true,
      bounciness: 0,
      restDisplacementThreshold: 40,
      restSpeedThreshold: 100,
    } as Animated.SpringAnimationConfig).start(clb);
  };

  const appearAddData = (clb?: Animated.EndCallback) => {
    Animated.spring(animatedSlidingCardAddDataSceneY, {
      toValue: 0,
      duration: 250,
      useNativeDriver: true,
      bounciness: 0,
      restDisplacementThreshold: 40,
      restSpeedThreshold: 100,
    } as Animated.SpringAnimationConfig).start(clb);
  };

  const disappearAddData = (clb?: Animated.EndCallback) => {
    Animated.spring(animatedSlidingCardAddDataSceneY, {
      toValue: SCENES[SceneTypes.ADD_DATA].height,
      duration: 250,
      useNativeDriver: true,
      bounciness: 0,
      restDisplacementThreshold: 40,
      restSpeedThreshold: 100,
    } as Animated.SpringAnimationConfig).start(clb);
  };

  /*
   * Utils
   */

  const goToScene = (sceneType: SceneTypes) => {
    switch (sceneType) {
      case SceneTypes.ADD_DATA:
        setInTransition(true)
        setScene(SceneTypes.ADD_DATA)
        disappearSelectCategory(() => setInTransition(false))
        appearAddData()
        break
      case SceneTypes.SELECT_CATEGORY:
        setInTransition(true)
        setScene(SceneTypes.SELECT_CATEGORY)
        disappearAddData(() => setInTransition(false))
        appearSelectCategory()
    }
  }

  return (
    <View>
      {(scene == SceneTypes.SELECT_CATEGORY || inTransition) &&
      <Animated.View style={[styles.panelAnimated, {
        position: inTransition ? "absolute" : "relative",
        transform: [{ translateY: animatedSlidingCardSelectCategorySceneY }]
      }]}>
          <AddTransactionSceneSelectCategoryComponent
            {...props}
            transactionMeta={transactionMeta}
            setTransactionMeta={setTransactionMeta}
            goToScene={goToScene}/>
      </Animated.View>}
      {(scene == SceneTypes.ADD_DATA || inTransition) &&
      <Animated.View style={[styles.panelAnimated, {
        position: inTransition ? "absolute" : "relative",
        transform: [{ translateY: animatedSlidingCardAddDataSceneY }]
      }]}>
          <AddTransactionSceneAddDataComponent
            {...props}
            transactionMeta={transactionMeta}
            setTransactionMeta={setTransactionMeta}
            goToScene={goToScene}/>
      </Animated.View>}
    </View>
  )
}

/*
 * State
 */

const mapStateToProps = (state: RootState) => {
  return {
    cache: state.cache
  };
};

// const mapDispatchToProps = (dispatch: Dispatch<StoreActionType>): DispatchFromProps => ({
// });

export const AddTransactionPopoverPanel = connect(mapStateToProps)(AddTransactionPopoverPanelComponent);

/*
 * Styles
 */

const generalStyles = StyleService.create({
  panelAnimated: {
    // position: "absolute",
    bottom: 0,
    marginBottom: 0,
    // zIndex: 3000
  },
})

const sceneSelectCategoryThemedStyles = StyleService.create({
  topPanel: {
    backgroundColor: "color-primary-500",
    height: sceneSelectCategoryTopPanelHeight,
    // @ts-ignore
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    paddingHorizontal: 20,
    paddingVertical: 8,
    marginHorizontal: 16
  },
  topPanelTop: {
    display: "flex",
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  topPanelBottom: {
    display: "flex",
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  buttonTransactionTypeSelected: {
    backgroundColor: "color-primary-200",
  },
  bottomPanel: {
    backgroundColor: "#ffffff",
    height: sceneSelectCategoryBottomPanelHeight,
    // @ts-ignore
    borderRadius: 16,
    paddingTop: 6
    // padding: 16
  },
  bottomPanelTabBar: { borderRadius: 16, backgroundColor: "#ffffffff" },
  gridView: {
    marginTop: 10,
    flex: 1,
  },
  itemContainer: {
    alignItems: "center",
    borderRadius: 60 / 2,
    height: 60,
    width: 60,
    justifyContent: "center"
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});

const sceneAddDataThemedStyles = StyleService.create({
  panel: {
    height: sceneAddDataTotalPanelHeight,
    backgroundColor: '#ffffff',
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    width: Dimensions.get("screen").width,
    flexDirection: "column",
  },
  directionsPanel: {
    borderTopLeftRadius: 12,
    display: "flex",
    flexDirection: "row",
    height: 70,
  },
  directionPanelItem: {
    padding: 12,
    flex: 1,
  },
  directionPanelItemRight: {
    borderTopRightRadius: 12,
  },
  directionPanelItemLeft: {
    borderTopLeftRadius: 12,
  },
  amountPanel: {
    flexDirection: "column",
    padding: 10,
    borderBottomColor: "#00000020",
    borderBottomWidth: 0.5
  },
  descriptionPanel: {
    padding: 12,
    // borderBottomColor: "#00000010",
    // borderBottomWidth: 1
  },
  keypadPanel: {},
  keypadRow: {
    flexDirection: "row"
  },
  keypadButtonText: {
    fontWeight: "300",
    fontSize: 22,
    color: '#00000090'
  },
  datePanel: {
    backgroundColor: '#00000005',
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    flexDirection: "column"
  },
  datePanelText: {
    textAlign: "center",
    fontWeight: "bold",
    color: "#00000060"
  }
});

const keypadStyles = StyleService.create({
  keypadPanel: {},
  keypadRow: {
    flexDirection: "row"
  },
  keypadButton: {
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 0.5,
    borderColor: '#00000020',
    height: Dimensions.get("screen").width / 5 - 10,
    width: Dimensions.get("screen").width / 5
  },
  keypadVerticalDoubleButton: {
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 0.5,
    borderColor: '#00000030',
    height: 2 * (Dimensions.get("screen").width / 5 - 10),
    width: Dimensions.get("screen").width / 5
  },
  keypadButtonText: {
    fontWeight: "300",
    fontSize: 26,
    color: '#00000090'
  },
  keypadFunctionButton: {
    backgroundColor: '#00000010'
  },
})

export const PopoverPanelStyle = {
  flexDirection: "row",
  alignItems: "flex-end"
} as StyleProp<ViewStyle>
