import { NavigationProp, RouteProp } from "@react-navigation/native";
import { Divider, Icon, StyleService, Text, TopNavigation, TopNavigationAction, useStyleSheet, useTheme } from "@ui-kitten/components";
import accounting from "accounting";
import Form from "lib/react-native-fields/components/Form";
import { FieldItemCaption, FieldItemDate, FieldItemHeader, FieldItemNumber, FieldItemSelect, FieldItemSelectValue, FieldItemText, FieldItemTextAutoComplete, FIELD_TYPE_CAPTION, FIELD_TYPE_DATE, FIELD_TYPE_HEADER, FIELD_TYPE_NUMBER, FIELD_TYPE_SELECT, FIELD_TYPE_TEXT, FIELD_TYPE_TEXT_AUTOCOMPLETE } from "lib/react-native-fields/types/fields";
import moment from "moment";
import React, { Dispatch, useState } from "react";
import { Alert, BackHandler, TouchableWithoutFeedback } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { connect } from "react-redux";
import { AccountAttributes } from "lib/fireflyiii-api/models/account";
import { API_DATE_FORMAT } from "lib/fireflyiii-api/models/api";
import { BudgetAttributes } from "lib/fireflyiii-api/models/budget";
import { CategoryAttributes } from "lib/fireflyiii-api/models/categories";
import { DataElement } from "lib/fireflyiii-api/models/data_elements";
import { TagAttributes } from "lib/fireflyiii-api/models/tags";
import { Transaction, TransactionAttributes, TransactionStoreElement, TRANSACTION_TYPE_DEPOSIT, TRANSACTION_TYPE_TRANSFER, TRANSACTION_TYPE_WITHDRAWAL } from "lib/fireflyiii-api/models/transaction";
import { ROUTE_ADD_TRANSACTION } from "src/routes/routesHome";
import { RootState } from "src/store/store";
import { StoreActionType } from "src/store/types";
import { transactionAdd } from "lib/fireflyiii-api/api";
import { BackIcon } from "src/utils/icons";
import { default as customMapping } from "../../../../custom-mapping.json";

/*
 * Interfaces
 */

interface Props {
  route: RouteProp<AddTransactionRouteParams, typeof ROUTE_ADD_TRANSACTION>;
  navigation: NavigationProp<AddTransactionRouteParams, typeof ROUTE_ADD_TRANSACTION>;

  // redux
  api_url: string
  token: string
  // cached
  cachedAccounts: Array<DataElement<AccountAttributes>>
  cachedTags: Array<DataElement<TagAttributes>>
  cachedCategories: Array<DataElement<CategoryAttributes>>
  cachedBudgets: Array<DataElement<BudgetAttributes>>
  // others
  cachedTransactions: Array<DataElement<TransactionAttributes>>
}

interface AddTransactionRouteParams {
  fromAccount: string
}

interface DispatchFromProps {
}

/*
 * Component
 */

const AddTransactionScreen = (props: Props) => {

  const styles = useStyleSheet(themedStyles);
  const theme = useTheme();

  React.useEffect(() => {
    setCategories(generateCategoriesList())
  }, [props.cachedCategories])

  /*
   * Utils
   */

  const generateAccountList = (): Array<FieldItemSelectValue> => {
    let accountsList = new Array<FieldItemSelectValue>()
    accountsList.push({ id: "0", value: "(cash)" }) // cash account has id=0
    props.cachedAccounts.forEach(account => {
      if (account.attributes.account_role === null) return
      accountsList.push({ id: account.id, value: `${account.attributes.name} (${accounting.formatMoney(account.attributes.current_balance, account.attributes.currency_symbol)})` })
    });
    return accountsList
  }

  const generateCategoriesList = (): Array<FieldItemSelectValue> => {
    let categoriesList = new Array<FieldItemSelectValue>()
    categoriesList.push({ id: "0", value: "(none)" })
    props.cachedCategories.forEach(c => {
      categoriesList.push({ id: c.id, value: c.attributes.name })
    })
    return categoriesList
  }

  const generateTagsList = (): Array<FieldItemSelectValue> => {
    let tagsList = new Array<FieldItemSelectValue>()
    tagsList.push({ id: "0", value: "(none)" })
    props.cachedTags.forEach(c => {
      tagsList.push({ id: c.id, value: c.attributes.tag })
    })
    return tagsList
  }

  const generateBudgetsList = (): Array<FieldItemSelectValue> => {
    let budgetList = new Array<FieldItemSelectValue>()
    budgetList.push({ id: "0", value: "(none)" })
    props.cachedBudgets.forEach(c => {
      budgetList.push({ id: c.id, value: c.attributes.name })
    })
    return budgetList
  }

  const pickDefaultIndexFromAccount = (): number => {
    if (props.route.params === undefined || props.route.params.fromAccount === undefined) return 0
    let accountsList = generateAccountList()
    for (let i = 0; i < accountsList.length; i++) {
      if (accountsList[i].id == props.route.params.fromAccount) return i;
    }
    return 0
  }

  /*
   * -> Suggestions
   */

  const generateTransactionSuggestedDescriptions = (value: string): Array<string> => {
    let suggestions = new Array<string>()
    if (value === undefined || value.length === 0 || value === "") return suggestions;
    props.cachedTransactions.forEach(t => {
      if (t.attributes.transactions[0].description.toLowerCase().includes(value.toLowerCase()) && suggestions.indexOf(t.attributes.transactions[0].description) < 0)
        suggestions.push(t.attributes.transactions[0].description)
    })
    return suggestions
  }

  const generateCategorySuggestions = (value: string): Array<FieldItemSelectValue> => {
    let categoriesSuggested = new Array<FieldItemSelectValue>()
    if (value === undefined || value.length === 0 || value === "") return categoriesSuggested;
    categories.forEach(c => {
      if (c.value.toLowerCase().includes(value.toLowerCase())) categoriesSuggested.push(c)
    })
    return categoriesSuggested
  }

  const getCategoryIdIfExisting = (value: string): number => {
    for (let i = 0; i < categories.length; i++) {
      if (categories[i].value.toLowerCase() === value.toLowerCase()) return parseInt(categories[i].id);
    }
    return -1
  }

  const transactionTypes = [
    { id: TRANSACTION_TYPE_WITHDRAWAL, value: "Withdrawal" },
    { id: TRANSACTION_TYPE_DEPOSIT, value: "Deposit" },
    { id: TRANSACTION_TYPE_TRANSFER, value: "Transfer" }
  ]

  /*
   * State
   */

  const [categories, setCategories] = useState(Array<FieldItemSelectValue>())

  // form
  const [formTransactionTypeSelected, setFormTransactionTypeSelected] = useState(0)
  const [formTransactionAmount, setFormTransactionAmount] = useState(0.00)
  const [formTransactionDescription, setFormTransactionDescription] = useState("")
  const [formTransactionSourceAccountIndex, setFormTransactionSourceAccountIndex] = useState(pickDefaultIndexFromAccount())
  const [formTransactionDestinationAccountIndex, setFormTransactionDestinationAccountIndex] = useState(0)
  const [formTransactionDate, setFormTransactionDate] = useState(new Date())

  const [formTransactionBudgetIndex, setFormTransactionBudgetIndex] = useState(0)
  const [formTransactionCategoryId, setFormTransactionCategoryId] = useState<number | null>(null)
  const [formTransactionCategory, setFormTransactionCategory] = useState<string | null>(null)
  // const [formTransactionTagIndex, setFormTransactionTagIndex] = useState(0)
  const [formTransactionTags, setFormTransactionTags] = useState<string | null>("")
  const [formTransactionInterestDate, setFormTransactionInterestDate] = useState(undefined)

  const [formTransactionDescriptionSuggestions, setFormTransactionDescriptionSuggestions] = useState(new Array<string>())
  const [formTransactionCategorySuggestions, setFormTransactionCategorySuggestions] = useState(new Array<FieldItemSelectValue>())


  const fieldsList = [
    {
      id: "type",
      type: FIELD_TYPE_SELECT,
      title: "Type",
      selected: formTransactionTypeSelected,
      options: transactionTypes, onSelectedValueChanged: (i) => setFormTransactionTypeSelected(i)
    } as FieldItemSelect,
    {
      id: "amount",
      type: FIELD_TYPE_NUMBER,
      title: "Amount",
      placeholder: "0.00",
      value: formTransactionAmount,
      onValueChanged: (v) => setFormTransactionAmount(v)
    } as FieldItemNumber,
    {
      id: "description",
      type: FIELD_TYPE_TEXT_AUTOCOMPLETE,
      title: "Description",
      placeholder: "e.g. Bought a new PC",
      value: formTransactionDescription,
      suggestions: formTransactionDescriptionSuggestions,
      onValueChanged: (v) => {
        setFormTransactionDescription(v)
        setFormTransactionDescriptionSuggestions(generateTransactionSuggestedDescriptions(v))
      },
      onSuggestionSelected: (i) => {
        setFormTransactionDescription(formTransactionDescriptionSuggestions[i])
        setFormTransactionDescriptionSuggestions(new Array<string>())
      },
      onSuggestionsBackdropPress: () => { setFormTransactionDescriptionSuggestions(new Array<string>()) },
    } as FieldItemTextAutoComplete,
    {
      id: "source-account",
      type: FIELD_TYPE_SELECT,
      title: "Source Account",
      selected: formTransactionSourceAccountIndex,
      options: generateAccountList(),
      onSelectedValueChanged: (i) => setFormTransactionSourceAccountIndex(i)
    } as FieldItemSelect,
    {
      id: "destination-account", type: FIELD_TYPE_SELECT, title: "Destination Account", selected: formTransactionDestinationAccountIndex, options: generateAccountList(), onSelectedValueChanged: (i) => setFormTransactionDestinationAccountIndex(i)
    } as FieldItemSelect,
    {
      id: "date",
      type: FIELD_TYPE_DATE,
      title: "Date",
      value: formTransactionDate,
      onDateSelected: (d) => d !== undefined ? setFormTransactionDate(d) : ""
    } as FieldItemDate,
    {
      id: "optional",
      type: FIELD_TYPE_HEADER,
      title: "Optional"
    } as FieldItemHeader,
    {
      id: "budget",
      type: FIELD_TYPE_SELECT,
      title: "Budget",
      selected: formTransactionBudgetIndex,
      options: generateBudgetsList(),
      onSelectedValueChanged: (i) => setFormTransactionBudgetIndex(i)
    } as FieldItemSelect,
    {
      id: "category",
      type: FIELD_TYPE_TEXT_AUTOCOMPLETE,
      title: "Category",
      placeholder: "(none)",
      value: formTransactionCategory,
      suggestions: formTransactionCategorySuggestions.map((c) => c.value),
      onValueChanged: (v) => {
        setFormTransactionCategory(v)
        setFormTransactionCategorySuggestions(generateCategorySuggestions(v))
        setFormTransactionCategoryId(null)
      },
      onSuggestionSelected: (i) => {
        setFormTransactionCategory(formTransactionCategorySuggestions[i].value)
        setFormTransactionCategoryId(parseInt(formTransactionCategorySuggestions[i].id))
        setFormTransactionCategorySuggestions(new Array<FieldItemSelectValue>())
      },
      onSuggestionsBackdropPress: () => {
        setFormTransactionCategorySuggestions(new Array<FieldItemSelectValue>())
        let possibleId = getCategoryIdIfExisting(formTransactionCategory)
        if (possibleId > 0) setFormTransactionCategoryId(possibleId)
      },
    } as FieldItemTextAutoComplete,
    {
      id: "tags",
      type: FIELD_TYPE_TEXT,
      title: "Tags",
      value: formTransactionTags,
      placeholder: "eg. salary, taxes",
      onValueChanged: (t) => { setFormTransactionTags(t) }
    } as FieldItemText,
    {
      id: "interest-date",
      type: FIELD_TYPE_DATE,
      title: "Interest Date",
      placeholderNotSet: "(not set)",
      value: formTransactionInterestDate,
      onDateSelected: (d) => setFormTransactionInterestDate(d)
    } as FieldItemDate,
    /*
     {
       id: "attachments",
       type: FIELD_TYPE_SELECT,
       title: "Attachments",
       selected: 0,
       options: [{ id: "0", value: "as" }]
     } as FieldItemSelect,
     */
    {
      id: "notes",
      type: FIELD_TYPE_TEXT,
      title: "Notes",
      placeholder: "e.g. PC specifications"
    } as FieldItemText,
    {
      id: "caption-add-fields",
      type: FIELD_TYPE_CAPTION,
      text: "Open Settings for enabling more transaction fields"
    } as FieldItemCaption,
  ]

  React.useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", () => { props.navigation.goBack(); return true })
    // console.log(eva.mapping.strict["text-font-family"])
    // console.log(props.route.params)
    // console.log(props.cachedTags)
    // console.log(strings.accountRoleCashWallet)
  }, [])

  /*
   * Renders
   */
  const renderTopNavigationBackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={() => props.navigation.goBack()} />
  );

  const renderIcon = (props) => (
    <Icon {...props} name={true ? 'eye-off' : 'eye'} />
  );

  const renderSaveAction = () => (
    <TouchableWithoutFeedback >
      <Text style={{ fontWeight: "bold", paddingEnd: 8 }} category="c2" onPress={() => submitForm()}>ADD</Text>
    </TouchableWithoutFeedback>
  )

  /*
   * Form logic
   */

  const submitForm = () => {
    Alert.alert("Add Transaction", "Are you sure to add this transaction?", [
      { text: "Cancel", style: "cancel" },
      { text: "OK", onPress: () => addTransactionApiCall() }
    ],
      { cancelable: true })
  }

  const validateForm = () => {

  }

  const addTransactionApiCall = () => {
    let transaction = {
      type: transactionTypes[formTransactionTypeSelected].id,
      amount: formTransactionAmount,
      description: formTransactionDescription,
      source_id: parseInt(generateAccountList()[formTransactionSourceAccountIndex].id),
      destination_id: parseInt(generateAccountList()[formTransactionDestinationAccountIndex].id),
      date: moment(formTransactionDate).format(API_DATE_FORMAT)
    } as Transaction
    // optional fields
    if (formTransactionInterestDate !== undefined)
      transaction.interest_date = moment(formTransactionInterestDate).format(API_DATE_FORMAT)
    if (formTransactionCategoryId !== null)
      transaction.category_id = formTransactionCategoryId
    if (formTransactionCategory !== null)
      transaction.category_name = formTransactionCategory
    if (formTransactionBudgetIndex !== 0)
      transaction.budget_id = formTransactionBudgetIndex
    if (formTransactionTags !== null && formTransactionTags !== "")
      transaction.tags = formTransactionTags.split(",")

    let transactionWrapper = {
      apply_rules: false,
      group_title: "",
      transactions: [transaction]
    } as TransactionStoreElement

    // make the api call
    transactionAdd(props.api_url, props.token, transactionWrapper)
      .then(res => {
        if (res.status === 200) {
          Alert.alert("Add Transaction", "Transaction added succesfully", [{ text: "OK", onPress: () => console.log("OK Pressed") }])
          console.log("transactionAdd: api ok")
          props.navigation.goBack();
        } else showApiErrorDialog(false, res.json());
      })
      .catch(err => {
        showApiErrorDialog(true, err)
      })
  }

  const showApiErrorDialog = (netError: boolean, err?: any) => {
    if (netError) {
      Alert.alert("Add Transaction", "There was an error reaching the server, please try again later", [{ text: "OK" }])
      return
    }
    err.then(json => {
      Alert.alert("Add Transaction", "There was an error adding the transaction please check that all fields are correct", [{ text: "OK", onPress: () => console.log("OK Pressed") }])
      console.log(`transactionAdd: api error: ${JSON.stringify(json)}`)
    }).catch(err => { })
  }

  /*
   * Subcomponents
   */


  /*
   * Component
   */

  return (
    <>
      <TopNavigation
        alignment="start"
        title="Add Transaction"
        subtitle="Creating a transaction to account ABC"
        accessoryLeft={renderTopNavigationBackAction}
        accessoryRight={renderSaveAction}
      />
      <Divider />
      <KeyboardAwareScrollView>
        <Form fieldItems={fieldsList} theme={theme} customMapping={customMapping} />
      </KeyboardAwareScrollView>
    </>
  )

}

/*
 * Store
 */

const mapStateToProps = (state: RootState) => ({
  api_url: state.config.api_url,
  token: state.auth.token,
  cachedAccounts: state.cache.accounts,
  cachedTags: state.cache.tags,
  cachedCategories: state.cache.categories,
  cachedBudgets: state.cache.budgets,
  // other
  cachedTransactions: state.cache.dashboard.transactions
});

const mapDispatchToProps = (dispatch: Dispatch<StoreActionType>): DispatchFromProps => ({

});

/*
 * Export
 */

export default connect(mapStateToProps, mapDispatchToProps)(AddTransactionScreen);

/*
 * Styles
 */

const themedStyles = StyleService.create({
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
});
