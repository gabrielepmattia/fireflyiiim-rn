import React from "react";
import { ScrollView } from "react-native";
import { connect } from "react-redux";
import { Divider, Layout, StyleService, Text, TopNavigation, useStyleSheet } from "@ui-kitten/components";

import { RootState } from "../../../store/store";
import { StoreActionType } from "../../../store/types";

/*
 * Interfaces
 */

export interface Props {
}

interface DispatchFromProps {
}

/*
 * Component
 */

const DebugScreen = () => {

  const styles = useStyleSheet(themedStyles);

  return (
    <>
      <TopNavigation alignment="center" title="Debug"/>
      <Divider/>
      <Layout level="2" style={{ flex: 1 }}>
        <ScrollView style={{padding: 16}}>
          <Text category="h1">Sample Text</Text>
          <Text category="h2">The Wonderful Wizard of Oz</Text>
          <Text category="h3">Chapter 11: The Wonderful Emerald City of Oz</Text>
          <Text category="h4">This is H4</Text>
          <Text category="h5">This is H5</Text>
          <Text category="p1">Even with eyes protected by the green spectacles, Dorothy and her
            friends were at first dazzled by the brilliancy of the wonderful City.
            The streets were lined with beautiful houses all built of green marble
            and studded everywhere with sparkling emeralds. They walked over a
            pavement of the same green marble, and where the blocks were joined
            together were rows of emeralds, set closely, and glittering in the
            brightness of the sun. The window panes were of green glass; even the
            sky above the City had a green tint, and the rays of the sun were green.</Text>
        </ScrollView>
      </Layout>
    </>
  )

}

/*
 * Export
 */

export default DebugScreen;

/*
 * Styles
 */

const themedStyles = StyleService.create({});
