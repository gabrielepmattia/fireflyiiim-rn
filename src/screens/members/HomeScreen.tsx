import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {
  BottomNavigation,
  BottomNavigationTab,
  Divider,
  StyleService,
  useStyleSheet,
  useTheme
} from '@ui-kitten/components';
import React, { useEffect, useState } from 'react';
import { BackHandler, SafeAreaView, StatusBar, View } from 'react-native';
import {
  ROUTE_ACCOUNT_INFO,
  ROUTE_ADD_TRANSACTION, ROUTE_DEBUG,
  ROUTE_HOME,
  ROUTE_HOME_ACCOUNTS,
  ROUTE_HOME_DASHBOARD,
  ROUTE_HOME_SETTINGS,
  ROUTE_HOME_TRANSACTIONS,
} from 'src/routes/routesHome';
import ModalComponent from "../../services/modal.component";
import ModalService from "../../services/modal.service";
import PopoverComponent from "../../services/popover.component";
import PopoverService from "../../services/popover.service";
import { CreditCardIcon, HomeIcon, SettingsIcon, SwapIcon } from '../../utils/icons';
import AddTransactionScreen from './screens/AddTransactionScreen';
import DebugScreen from "./screens/DebugScreen";
import AccountInfoScreen from './tabs/accounts/AccountInfoScreen';
import AccountsTab from './tabs/AccountsTab';
import DashboardTab from './tabs/DashboardTab';
import SettingsTab from './tabs/SettingsTab';
import TransactionsTab from './tabs/TransactionsTab';

/*
 * Main Home Stack Navigator
 */

const HomeScreen = () => {
  const theme = useTheme();
  const Stack = createStackNavigator();

  return (
    <>
      <SafeAreaView style={{ flex: 0, backgroundColor: theme['background-basic-color-1'] }}/>
      <StatusBar barStyle="dark-content" backgroundColor="#fff"/>
      <SafeAreaView style={{ flex: 1 }}>
        <NavigationContainer independent={true}>
          <Stack.Navigator headerMode="none">
            <Stack.Screen name={ROUTE_HOME} component={HomeScreenComponent}/>
            <Stack.Screen name={ROUTE_ACCOUNT_INFO} component={AccountInfoScreen}/>
            <Stack.Screen name={ROUTE_ADD_TRANSACTION} component={AddTransactionScreen}/>
            <Stack.Screen name={ROUTE_DEBUG} component={DebugScreen}/>
          </Stack.Navigator>
        </NavigationContainer>
        { /* Over components */}
        <PopoverComponent/>
        <ModalComponent/>
      </SafeAreaView>
      <SafeAreaView style={{ flex: 0, backgroundColor: theme['background-basic-color-1'] }}/>
    </>
  );
};

export default HomeScreen;

/*
 * HomeScreen Component
 */

const HomeScreenComponent = (): React.ReactElement => {
  const theme = useTheme();
  const styles = useStyleSheet(themedStyles);

  const Tab = createBottomTabNavigator();

  const [initBack, setInitBack] = useState(false)

  useEffect(() => {
    if (initBack) return
    // init back button behavior only once
    BackHandler.addEventListener('hardwareBackPress', function () {
      if (ModalService.getInstance().isShown()) {
        ModalService.getInstance().show(false)
        return true;
      }

      if (PopoverService.getInstance().isShown()) {
        PopoverService.getInstance().show(false)
        return true;
      }

      return false;
    })

    setInitBack(true)
  }, []);

  const BottomTabBar = ({ navigation, state }) => (
    <View>
      <Divider/>
      <BottomNavigation
        indicatorStyle={styles.bottomIndicator}
        selectedIndex={state.index}
        onSelect={(index) => navigation.navigate(state.routeNames[index])}>
        <BottomNavigationTab title="Dashboard" icon={HomeIcon}/>
        <BottomNavigationTab title="Accounts" icon={CreditCardIcon}/>
        <BottomNavigationTab title="Transactions" icon={SwapIcon}/>
        <BottomNavigationTab title="Settings" icon={SettingsIcon}/>
      </BottomNavigation>
    </View>
  );

  return (
    <Tab.Navigator backBehavior="none" tabBar={(props) => <BottomTabBar {...props} />}>
      <Tab.Screen name={ROUTE_HOME_DASHBOARD} component={DashboardTab}/>
      <Tab.Screen name={ROUTE_HOME_ACCOUNTS} component={AccountsTab}/>
      <Tab.Screen name={ROUTE_HOME_TRANSACTIONS} component={TransactionsTab}/>
      <Tab.Screen name={ROUTE_HOME_SETTINGS} component={SettingsTab}/>
    </Tab.Navigator>
  );
};

const themedStyles = StyleService.create({
  bottomIndicator: {
    borderRadius: 6,
    backgroundColor: 'transparent',
  },
});
