/*
 * HomeScreen routes
 */

export const ROUTE_HOME = "Home"

export const ROUTE_HOME_DASHBOARD = "Dashboard"
export const ROUTE_HOME_ACCOUNTS = "Accounts"
export const ROUTE_HOME_TRANSACTIONS = "Transactions"
export const ROUTE_HOME_SETTINGS = "Settings"

export const ROUTE_HOME_ACCOUNTS_TABS = "AccountsTabs"

export const ROUTE_HOME_ACCOUNTS_TAB_ASSETS = "AccountsTabAssets"
export const ROUTE_HOME_ACCOUNTS_TAB_EXPENSE = "AccountsTabExpense"
export const ROUTE_HOME_ACCOUNTS_TAB_REVENUE = "AccountsTabRevenue"
export const ROUTE_HOME_ACCOUNTS_TAB_LIABILITIES = "AccountsTabLiabilities"

export const ROUTE_ACCOUNT_INFO = "AccountInfo"
export const ROUTE_ADD_TRANSACTION = "AddTransaction"
export const ROUTE_DEBUG = "Debug"
