import { SET_APP_LOADED_PERSISTENT } from './types';
import { AppActionType } from 'src/store/app/types';

export function setAppLoadedPersistent(loaded: boolean): AppActionType {
  return {
    type: SET_APP_LOADED_PERSISTENT,
    payload: loaded
  }
}