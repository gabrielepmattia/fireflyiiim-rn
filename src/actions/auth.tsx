import { SET_AUTH_TOKEN, SET_AUTH_SIGNED } from './types';
import { AuthActionType } from 'src/store/auth/types';

export function setAuthToken(token: string): AuthActionType {
  return {
    type: SET_AUTH_TOKEN,
    payload: token
  }
}

export function setAuthSignedIn(signed: boolean): AuthActionType {
  return {
    type: SET_AUTH_SIGNED,
    payload: signed
  }
}