import { SET_CONFIG_API_URL } from './types';
import { ConfigActionType } from 'src/store/config/types';

export function setConfigApiUrl(api_url: string): ConfigActionType {
  return {
    type: SET_CONFIG_API_URL,
    payload: api_url
  }
}