import { AboutAttributes } from "lib/fireflyiii-api/models/about"
import { AccountAttributes } from "lib/fireflyiii-api/models/account"
import { BudgetAttributes } from "lib/fireflyiii-api/models/budget"
import { CategoryAttributes } from "lib/fireflyiii-api/models/categories"
import { DataElement } from "lib/fireflyiii-api/models/data_elements"
import { Summary } from "lib/fireflyiii-api/models/summary"
import { TagAttributes } from "lib/fireflyiii-api/models/tags"
import { TransactionAttributes } from "lib/fireflyiii-api/models/transaction"
import { CacheActionType } from "src/store/cache/types"
import { SET_CACHE_ABOUT, SET_CACHE_ACCOUNTS_LIST, SET_CACHE_BUDGETS, SET_CACHE_CATEGORIES, SET_CACHE_CLEAN, SET_CACHE_DASHBOARD_SUMMARY, SET_CACHE_DASHBOARD_TRANSACTIONS, SET_CACHE_TAGS } from "./types"


export function setCacheAccounts(accountList: Array<DataElement<AccountAttributes>>): CacheActionType {
  return {
    type: SET_CACHE_ACCOUNTS_LIST,
    payload: accountList
  }
}

export function setCacheDashboardSummary(summary: Summary): CacheActionType {
  return {
    type: SET_CACHE_DASHBOARD_SUMMARY,
    payload: summary
  }
}

export function setCacheDashboardTransactions(transactions: Array<DataElement<TransactionAttributes>>): CacheActionType {
  return {
    type: SET_CACHE_DASHBOARD_TRANSACTIONS,
    payload: transactions
  }
}

export function setCacheCategories(categories: Array<DataElement<CategoryAttributes>>): CacheActionType {
  return {
    type: SET_CACHE_CATEGORIES,
    payload: categories
  }
}

export function setCacheTags(tags: Array<DataElement<TagAttributes>>): CacheActionType {
  return {
    type: SET_CACHE_TAGS,
    payload: tags
  }
}

export function setCacheBudgets(budgets: Array<DataElement<BudgetAttributes>>): CacheActionType {
  return {
    type: SET_CACHE_BUDGETS,
    payload: budgets
  }
}

export function setCacheAbout(about: AboutAttributes): CacheActionType {
  return {
    type: SET_CACHE_ABOUT,
    payload: about
  }
}

export function setCacheClean(): CacheActionType {
  return {
    type: SET_CACHE_CLEAN
  }
}
