export const SET_AUTH_TOKEN = 'SET_AUTH_TOKEN';
export const SET_AUTH_SIGNED = 'SET_AUTH_SIGNED';

export const SET_APP_LOADED_PERSISTENT = 'SET_APP_LOADED_PERSISTENT';

export const SET_CONFIG_API_URL = 'SET_APP_LOADED';
export const SET_CONFIG_DEAFULT_DASHBOARD_START_DATE = 'SET_APP_LOADED';
export const SET_CONFIG_DEAFULT_DASHBOARD_END_DATE = 'SET_APP_LOADED';

export const SET_CACHE_ACCOUNTS_LIST = 'SET_CACHE_ACCOUNTS_LIST'
export const SET_CACHE_DASHBOARD_SUMMARY = 'SET_CACHE_DASHBOARD_SUMMARY'
export const SET_CACHE_DASHBOARD_TRANSACTIONS = 'SET_CACHE_DASHBOARD_TRANSACTIONS'
export const SET_CACHE_CATEGORIES = "SET_CACHE_CATEGORIES";
export const SET_CACHE_TAGS = "SET_CACHE_TAGS";
export const SET_CACHE_BUDGETS = "SET_CACHE_BUDGETS";
export const SET_CACHE_CLEAN = "SET_CACHE_CLEAN";
export const SET_CACHE_ABOUT = "SET_CACHE_ABOUT";
