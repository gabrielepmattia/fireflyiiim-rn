const DEFAULT_CATEGORIES = ["salary", "leisure", "transport", "family", "health", "groceries", "gifts", "shopping", "travels"]

export const getDefaultCategories = (): Array<string> => {
  return DEFAULT_CATEGORIES
}

