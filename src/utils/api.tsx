export const retrieveDemoCredentials = (): Promise<Response> => {
  return fetch('https://raw.gabrielepmattia.com/fireflyiiim/demo_login.json');
};
