import { Icon } from '@ui-kitten/components';
import React from 'react';
import { ImageProps } from 'react-native';

export const HeartIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name="heart"/>;

export const EyeIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name="heart"/>

export const EyeOffIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name="heart"/>;

export const PersonIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name="heart"/>;

export const HomeIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name="home"/>;

export const CreditCardIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name="credit-card"/>;

export const SwapIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name="swap"/>;

export const SettingsIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name="settings"/>;

export const DiagonalArrowRightUp = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name="diagonal-arrow-right-up"/>;

export const DiagonalArrowRightDown = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name="diagonal-arrow-right-down"/>;

export const EditIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name='edit'/>

export const MenuIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name='more-vertical'/>

export const InfoIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name='info'/>

export const LogoutIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name='log-out'/>

export const BackIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name='arrow-back'/>

export const CalendarIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name='calendar'/>

export const ShoppingCartIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name='shopping-cart'/>

export const PricetagsIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name='pricetags'/>

export const CodeIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name='code'/>

export const HardDriveIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name='hard-drive'/>

export const PlusIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name='plus'/>

export const ChevronRightIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name='chevron-right'/>

export const ArrowDownwardOutline = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name='arrow-downward-outline'/>

export const ArrowUpwardOutline = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name='arrow-upward-outline'/>

export const MenuArrowOutline = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> =>
  <Icon {...props} name='menu-arrow-outline'/>
