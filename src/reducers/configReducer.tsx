import { SET_CONFIG_API_URL } from '../actions/types';
import { ConfigState, ConfigActionType } from 'src/store/config/types';

const initialState = {
  api_url: '',
};

function configReducer(state = initialState, action: ConfigActionType): ConfigState {
  switch (action.type) {
    case SET_CONFIG_API_URL:
      return {
        ...state,
        api_url: action.payload,
      };
    default:
      return state;
  }
}

export default configReducer;
