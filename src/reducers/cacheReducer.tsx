import { AboutAttributes } from 'lib/fireflyiii-api/models/about';
import { AccountAttributes } from 'lib/fireflyiii-api/models/account';
import { BudgetAttributes } from 'lib/fireflyiii-api/models/budget';
import { CategoryAttributes } from 'lib/fireflyiii-api/models/categories';
import { DataElement } from 'lib/fireflyiii-api/models/data_elements';
import { Summary } from 'lib/fireflyiii-api/models/summary';
import { TagAttributes } from 'lib/fireflyiii-api/models/tags';
import { TransactionAttributes } from 'lib/fireflyiii-api/models/transaction';
import { CacheActionType, CacheState } from 'src/store/cache/types';
import { SET_CACHE_ABOUT, SET_CACHE_ACCOUNTS_LIST, SET_CACHE_BUDGETS, SET_CACHE_CATEGORIES, SET_CACHE_CLEAN, SET_CACHE_DASHBOARD_SUMMARY, SET_CACHE_DASHBOARD_TRANSACTIONS, SET_CACHE_TAGS } from '../actions/types';

const initialState = {
  about: {} as AboutAttributes,
  accounts: new Array<DataElement<AccountAttributes>>(),
  dashboard: {
    transactions: new Array<DataElement<TransactionAttributes>>(),
    summary: {} as Summary
  },
  categories: new Array<DataElement<CategoryAttributes>>(),
  tags: new Array<DataElement<TagAttributes>>(),
  budgets: new Array<DataElement<BudgetAttributes>>()
};

function cacheReducer(state = initialState, action: CacheActionType): CacheState {
  switch (action.type) {
    case SET_CACHE_ACCOUNTS_LIST:
      return {
        ...state,
        accounts: action.payload,
      };
    case SET_CACHE_DASHBOARD_SUMMARY:
      return {
        ...state,
        dashboard: {
          ...state.dashboard,
          summary: action.payload
        }
      }
    case SET_CACHE_DASHBOARD_TRANSACTIONS:
      return {
        ...state,
        dashboard: {
          ...state.dashboard,
          transactions: action.payload
        }
      }
    case SET_CACHE_CATEGORIES:
      return {
        ...state,
        categories: action.payload
      }
    case SET_CACHE_TAGS:
      return {
        ...state,
        tags: action.payload
      }
    case SET_CACHE_BUDGETS:
      return {
        ...state,
        budgets: action.payload
      }
    case SET_CACHE_ABOUT:
      return {
        ...state,
        about: action.payload
      }
    case SET_CACHE_CLEAN:
      return {
        ...initialState,
      }
    default:
      return state;
  }
}

export default cacheReducer;
