import { SET_AUTH_TOKEN, SET_AUTH_SIGNED } from '../actions/types';
import { AuthActionType, AuthState } from 'src/store/auth/types';

const initialState = {
  signed_in: false,
  token: '',
};

function authReducer(state = initialState, action: AuthActionType): AuthState {
  switch (action.type) {
    case SET_AUTH_TOKEN:
      return {
        ...state,
        token: action.payload,
      };
    case SET_AUTH_SIGNED:
      return {
        ...state,
        signed_in: action.payload,
      };
    default:
      return state;
  }
}

export default authReducer;
