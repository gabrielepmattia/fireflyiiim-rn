import { SET_APP_LOADED_PERSISTENT } from '../actions/types';
import { AppActionType, AppState } from 'src/store/app/types';

const initialState = {
  loadedPersistent: false,
};

function appReducer(state = initialState, action: AppActionType): AppState {
  switch (action.type) {
    case SET_APP_LOADED_PERSISTENT:
      return {
        ...state,
        loadedPersistent: action.payload,
      };
    default:
      return state;
  }
}

export default appReducer;
