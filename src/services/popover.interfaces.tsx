export interface PopoverPanel {
  show(): void

  hide(): void
}

export interface PopoverPanelProps {
  visibility: boolean
  close: () => void
}
