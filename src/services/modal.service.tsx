import React from "react";
import { StyleProp, ViewStyle } from "react-native";
import ModalComponent from "./modal.component";
import { ModalPanelProps } from "./modal.interfaces";

export default class ModalService {
  private static instance: ModalService = new ModalService()

  private modalComponent: ModalComponent | null = null
  private panel: React.FC<ModalPanelProps> | null = null
  private panelProps: ModalPanelProps | null = null
  private panelContainerStyle: StyleProp<ViewStyle> | null = null

  public static getInstance(): ModalService {
    if (ModalService.instance == null) ModalService.instance = new ModalService()
    return ModalService.instance
  }

  /*
   * Setters
   */

  setModalPanelStyle(style: StyleProp<ViewStyle>) {
    this.panelContainerStyle = style
    this.modalComponent?.initPanelStyle()
  }

  setModalPanelProps(props: ModalPanelProps) {
    this.panelProps = props
    this.modalComponent?.initPanelProps()
  }

  setModalPanel(panel: React.FC<ModalPanelProps>) {
    this.panelContainerStyle = null
    this.panel = panel
    this.modalComponent?.initPanel()
  }

  /*
   * Getters
   */

  getModalPanelStyle(): StyleProp<ViewStyle> | null {
    return this.panelContainerStyle ? this.panelContainerStyle : null
  }

  getModalPanelProps(): ModalPanelProps | null {
    return this.panelProps
  }

  getModalPanel(): React.FC<ModalPanelProps> | null {
    return this.panel ? this.panel : null
  }

  isShown() {
    if (this.modalComponent == undefined) return false
    return this.modalComponent?.isShown()
  }

  /*
   * Core
   */

  mount(component: ModalComponent) {
    this.modalComponent = component
  }

  unmount() {
    this.modalComponent = null
  }

  show(visible: boolean): boolean {
    if (this.modalComponent == null) return false
    this.modalComponent.show(visible)
    return true
  }

}
