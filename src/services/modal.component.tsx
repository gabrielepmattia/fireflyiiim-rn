import { StyleService } from "@ui-kitten/components";
import React from "react";
import { Animated, StyleProp, TouchableWithoutFeedback, View, ViewProps, ViewStyle } from "react-native";
import { ModalPanelProps } from "./modal.interfaces";
import ModalService from "./modal.service";

export interface ModalFunctionalProps {
  visibility?: boolean
  close?: () => void
}

interface ModalComponentState {
  visible: boolean
  panel: React.FC<ModalPanelProps> | null
  panelStyle: StyleProp<ViewStyle> | null
  panelVisibility: boolean
  panelProps: ModalPanelProps | null
}

export default class ModalComponent extends React.Component<ModalPanelProps, ModalComponentState> {

  state = {
    visible: false,
    panel: null,
    panelStyle: null,
    panelProps: null,
    panelVisibility: false
  } as ModalComponentState

  // styles = useStyleSheet(themedStyles);
  // theme = useTheme();

  animatedBackdropOpacity = new Animated.Value(0);
  animatedModalPanelOpacity = new Animated.Value(0);

  constructor(props: ModalPanelProps) {
    super(props);
  }

  public componentDidMount(): void {
    ModalService.getInstance().mount(this);
  }

  public componentWillUnmount(): void {
    ModalService.getInstance().unmount();
  }

  public initPanel() {
    this.setState({
      panel: ModalService.getInstance().getModalPanel(),
    })
  }

  public initPanelStyle() {
    this.setState({
      panelStyle: ModalService.getInstance().getModalPanelStyle(),
    })
  }

  public initPanelProps() {
    this.setState({
      panelProps: ModalService.getInstance().getModalPanelProps(),
    })
  }

  public show(visible: boolean) {
    if (this.state.visible) {
      this.setState({ panelVisibility: false })
      this.modalPanelFadeOut()
      this.backdropFadeOut(() => {
        this.setState({ visible })
      });
    } else {
      this.setState({ visible, panelVisibility: true })
      this.backdropFadeIn();
      this.modalPanelFadeIn();
    }
  }

  public isShown() {
    return this.state.visible
  }

  public closeModal = () => {
    this.show(false)
  }

  public render(): React.ReactElement<ViewProps> | false {
    return this.state.visible && (
      <View style={[themedStyles.popOverContainer, this.state.panelStyle]}>
        <TouchableWithoutFeedback style={themedStyles.backdropContainer} onPress={() => this.show(false)}>
          <Animated.View style={[themedStyles.backdrop, { opacity: this.animatedBackdropOpacity },]}/>
        </TouchableWithoutFeedback>
        <Animated.View style={[themedStyles.modalPanelContainer, { opacity: this.animatedModalPanelOpacity }]}>
          {this.state.panel != null && <this.state.panel
            {...this.state.panelProps} visibility={this.state.panelVisibility} close={this.closeModal}/>}
        </Animated.View>
      </View>
    );
  }

  /*
   * Animations
   */

  private backdropFadeIn(clb?: Animated.EndCallback) {
    Animated.timing(this.animatedBackdropOpacity, {
      toValue: 1,
      duration: 150,
      useNativeDriver: true,
    } as Animated.TimingAnimationConfig).start(clb);
  };

  private backdropFadeOut(clb?: Animated.EndCallback) {
    Animated.timing(this.animatedBackdropOpacity, {
      toValue: 0,
      duration: 150,
      useNativeDriver: true,
    } as Animated.TimingAnimationConfig).start(clb);
  };

  private modalPanelFadeIn(clb?: Animated.EndCallback) {
    Animated.timing(this.animatedModalPanelOpacity, {
      toValue: 1,
      duration: 150,
      useNativeDriver: true,
    } as Animated.TimingAnimationConfig).start(clb);
  };

  private modalPanelFadeOut(clb?: Animated.EndCallback) {
    Animated.timing(this.animatedModalPanelOpacity, {
      toValue: 0,
      duration: 150,
      useNativeDriver: true,
    } as Animated.TimingAnimationConfig).start(clb);
  };


}

/*
 * Styles
 */

const themedStyles = StyleService.create({
  popOverContainer: {
    position: 'absolute',
    zIndex: 1000,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: "center"
  },
  backdropContainer: {
    zIndex: 0, position: "absolute", top: 0, left: 0, right: 0
  },
  backdrop: {
    position: "absolute",
    zIndex: 1500,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
  modalPanelContainer: {
    backgroundColor: "#ffffff",
    borderRadius: 12,
    zIndex: 2000,
    marginHorizontal: 26,
    paddingHorizontal: 24,
    paddingVertical: 16,
  }
});

