import { StyleService } from "@ui-kitten/components";
import React from "react";
import { Animated, BackHandler, StyleProp, TouchableWithoutFeedback, View, ViewProps, ViewStyle } from "react-native";
import ModalService from "./modal.service";
import { PopoverPanelProps } from "./popover.interfaces";
import PopoverService from "./popover.service";


interface PopoverComponentProps {

}


interface PopoverComponentState {
  visible: boolean
  panel: React.FC<PopoverPanelProps> | null
  panelStyle: StyleProp<ViewStyle> | null
  panelVisibility: boolean
}

export default class PopoverComponent extends React.Component<PopoverComponentProps, PopoverComponentState> {

  state = { visible: false, panel: null, panelStyle: null, panelVisibility: false } as PopoverComponentState

  // styles = useStyleSheet(themedStyles);
  // theme = useTheme();

  animatedBackdropOpacity = new Animated.Value(0);
  animatedSlidingCardY = new Animated.Value(0);

  constructor(props: PopoverComponentProps) {
    super(props);
  }

  public componentDidMount(): void {
    PopoverService.getInstance().mount(this);
  }

  public componentWillUnmount(): void {
    PopoverService.getInstance().unmount();
  }

  public initPanel() {
    this.setState({
      panel: PopoverService.getInstance().getPopoverPanel(),
    })
  }

  public initPanelStyle() {
    this.setState({
      panelStyle: PopoverService.getInstance().getPopoverPanelStyle(),
    })
  }

  public show(visible: boolean) {
    if (this.state.visible) {
      this.setState({ panelVisibility: false })
      this.fadeOut(() => {
        this.setState({ visible })
      });
    } else {
      this.setState({ visible, panelVisibility: true })
      this.fadeIn();
    }
  }

  public close() {
    this.show(false)
  }

  public isShown() {
    return this.state.visible
  }

  public render(): React.ReactElement<ViewProps> | false {
    return this.state.visible && (
      <View style={[themedStyles.popOverContainer, this.state.panelStyle]}>
        <TouchableWithoutFeedback style={{ zIndex: 0, position: "absolute", top: 0, left: 0, right: 0 }}
                                  onPress={() => this.show(false)}>
          <Animated.View style={[themedStyles.backdropContainer, { opacity: this.animatedBackdropOpacity },]}/>
        </TouchableWithoutFeedback>
        <View style={{ ...themedStyles.panelContainer }}>
          {this.state.panel != null &&
          <this.state.panel close={() => this.show(false)} visibility={this.state.panelVisibility}/>}
        </View>
      </View>
    );
  }

  /*
   * Animations
   */

  private fadeIn(clb?: Animated.EndCallback) {
    Animated.timing(this.animatedBackdropOpacity, {
      toValue: 1,
      duration: 200,
      useNativeDriver: true,
    } as Animated.TimingAnimationConfig).start(clb);
  };

  private fadeOut(clb?: Animated.EndCallback) {
    Animated.timing(this.animatedBackdropOpacity, {
      toValue: 0,
      duration: 200,
      useNativeDriver: true,
    } as Animated.TimingAnimationConfig).start(clb);
  };

}

/*
 * Styles
 */

const themedStyles = StyleService.create({
  popOverContainer: {
    position: 'absolute',
    zIndex: 1000,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  backdropContainer: {
    position: "absolute",
    zIndex: 1500,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
  panelContainer: {
    zIndex: 2000,
  }
});

