import { ModalPanelInputProps } from "../screens/members/components/modals/ModalPanelInputComponent";
import { ModalPanelMessageDismissProps } from "../screens/members/components/modals/ModalPanelMessageDismissComponent";
import { ModalPanelMessagePNProps } from "../screens/members/components/modals/ModalPanelMessagePNComponent";

export interface ModalPanel {
  show(): void

  hide(): void
}

export type ModalPanelProps = ModalPanelInputProps | ModalPanelMessagePNProps | ModalPanelMessageDismissProps
