import { FireflyIIIApi } from "../../lib/fireflyiii-api/api";
import { store } from "../store/store";

const MODULE = "ApiService"

export default class ApiService {

  private static instance: ApiService = new ApiService()
  private static api: FireflyIIIApi

  private currentApiUrl = ""
  private currentToken = ""

  constructor() {
    console.debug(`[${MODULE}] constructor called`)
    ApiService.api = new FireflyIIIApi("", "")

    const storeState = store.getState()
    this.currentApiUrl = storeState.config.api_url
    this.currentToken = storeState.auth.token

    console.debug(`[${MODULE}] this.currentApiUrl=${this.currentApiUrl}`)
    console.debug(`[${MODULE}] this.token=${this.currentToken}`)

    ApiService.api.setAuthData(this.currentApiUrl, this.currentToken)

    console.debug(`[${MODULE}] ApiService.api.apiUrl=${ApiService.api.getApiUrl()}`)
    console.debug(`[${MODULE}] ApiService.api.token=${ApiService.api.getToken()}`)

    // listen to auth value changes
    store.subscribe(this.updateAuthDataListener)
  }

  public static getInstance(): ApiService {
    if (ApiService.instance == null) ApiService.instance = new ApiService()
    return ApiService.instance
  }

  public handler(): FireflyIIIApi {
    return ApiService.api
  }

  private updateAuthDataListener() {
    const storeState = store.getState()
    if (this.currentToken === storeState.auth.token && this.currentApiUrl === storeState.config.api_url) return
    console.debug(`[${MODULE}] updateAuthDataListener called ${ApiService.getInstance().handler()}`)
    // update the auth data
    ApiService.api.setAuthData(storeState.config.api_url, storeState.auth.token)
    this.currentApiUrl = storeState.config.api_url
    this.currentToken = storeState.auth.token
  }
}
