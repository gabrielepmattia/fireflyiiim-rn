import React from "react";
import { StyleProp, ViewStyle } from "react-native";
import PopoverComponent from "./popover.component";
import { PopoverPanelProps } from "./popover.interfaces";

export default class PopoverService {
  private static instance: PopoverService = new PopoverService()
  private popoverComponent: PopoverComponent | null = null
  private panel: React.FC<PopoverPanelProps> | null = null
  private panelContainerStyle: StyleProp<ViewStyle> | null = null

  public static getInstance(): PopoverService {
    if (PopoverService.instance == null) PopoverService.instance = new PopoverService()
    return PopoverService.instance
  }

  /*
   * Setters
   */

  setPopoverPanelStyle(style: StyleProp<ViewStyle>) {
    this.panelContainerStyle = style
    this.popoverComponent?.initPanelStyle()
  }

  setPopoverPanel(panel: React.FC<PopoverPanelProps>) {
    this.panelContainerStyle = null
    this.panel = panel
    this.popoverComponent?.initPanel()
  }

  /*
   * Getters
   */

  getPopoverPanelStyle(): StyleProp<ViewStyle> | null {
    return this.panelContainerStyle ? this.panelContainerStyle : null
  }

  getPopoverPanel(): React.FC<PopoverPanelProps> | null {
    return this.panel ? this.panel : null
  }

  isShown(): boolean {
    if (this.popoverComponent == undefined) return false
    return this.popoverComponent?.isShown()
  }

  /*
   * Core
   */

  mount(component: PopoverComponent) {
    this.popoverComponent = component
  }

  unmount() {
    this.popoverComponent = null
  }

  show(visible: boolean): boolean {
    if (this.popoverComponent == null) return false
    this.popoverComponent.show(visible)
    return true
  }

}
