import { StatusBar } from "react-native";

export default class StatusbarService {
  private static instance: StatusbarService = new StatusbarService()
  private statusBars: Array<StatusBar> = []

  public static getInstance(): StatusbarService {
    if (StatusbarService.instance == null) StatusbarService.instance = new StatusbarService()
    return StatusbarService.instance
  }

  mountBar() {

  }
}
