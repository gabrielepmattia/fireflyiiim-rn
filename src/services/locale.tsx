// ES6 module syntax
import LocalizedStrings from 'react-native-localization';
import { default as localeEN } from '../../locales/en.json';
import { default as localeIT } from '../../locales/it.json';


// CommonJS syntax
// let LocalizedStrings  = require ('react-native-localization');

export const strings = new LocalizedStrings({
  en: localeEN,
  "en-US": localeEN,
  it: localeIT
});