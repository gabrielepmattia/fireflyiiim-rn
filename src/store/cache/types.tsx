import { SET_CACHE_ABOUT, SET_CACHE_ACCOUNTS_LIST, SET_CACHE_BUDGETS, SET_CACHE_CATEGORIES, SET_CACHE_CLEAN, SET_CACHE_DASHBOARD_SUMMARY, SET_CACHE_DASHBOARD_TRANSACTIONS, SET_CACHE_TAGS } from "src/actions/types";
import { AboutAttributes } from "lib/fireflyiii-api/models/about";
import { AccountAttributes } from "lib/fireflyiii-api/models/account";
import { BudgetAttributes } from "lib/fireflyiii-api/models/budget";
import { CategoryAttributes } from "lib/fireflyiii-api/models/categories";
import { DataElement } from "lib/fireflyiii-api/models/data_elements";
import { Summary } from "lib/fireflyiii-api/models/summary";
import { TagAttributes } from "lib/fireflyiii-api/models/tags";
import { TransactionAttributes } from "lib/fireflyiii-api/models/transaction";

export interface CacheState {
  about: AboutAttributes,
  accounts: Array<DataElement<AccountAttributes>>
  dashboard: CacheDashboardState
  categories: Array<DataElement<CategoryAttributes>>
  tags: Array<DataElement<TagAttributes>>
  budgets: Array<DataElement<BudgetAttributes>>
}

export interface CacheDashboardState {
  transactions: Array<DataElement<TransactionAttributes>>
  summary: Summary
}

/*
 * Actions
 */

export interface SetCacheAccountsListAction {
  type: typeof SET_CACHE_ACCOUNTS_LIST;
  payload: Array<DataElement<AccountAttributes>>;
}

export interface SetCacheDashboardSummaryAction {
  type: typeof SET_CACHE_DASHBOARD_SUMMARY;
  payload: Summary;
}

export interface SetCacheTransactionsAction {
  type: typeof SET_CACHE_DASHBOARD_TRANSACTIONS;
  payload: Array<DataElement<TransactionAttributes>>;
}

export interface SetCacheCategoriesAction {
  type: typeof SET_CACHE_CATEGORIES;
  payload: Array<DataElement<CategoryAttributes>>;
}

export interface SetCacheTagsAction {
  type: typeof SET_CACHE_TAGS;
  payload: Array<DataElement<TagAttributes>>;
}

export interface SetCacheBudgetsAction {
  type: typeof SET_CACHE_BUDGETS;
  payload: Array<DataElement<BudgetAttributes>>;
}

export interface SetCacheAboutAction {
  type: typeof SET_CACHE_ABOUT;
  payload: AboutAttributes;
}


export interface SetCacheClean {
  type: typeof SET_CACHE_CLEAN;
}


export type CacheActionType = SetCacheAccountsListAction | SetCacheDashboardSummaryAction | SetCacheTransactionsAction | SetCacheCategoriesAction | SetCacheTagsAction | SetCacheBudgetsAction | SetCacheAboutAction | SetCacheClean
