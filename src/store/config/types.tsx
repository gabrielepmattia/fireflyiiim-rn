import { SET_CONFIG_API_URL } from "src/actions/types";

export interface ConfigState {
  api_url: string
}

/*
 * Actions
 */

interface SetConfigApiUrlAction {
  type: typeof SET_CONFIG_API_URL
  payload: string
}

export type ConfigActionType = SetConfigApiUrlAction