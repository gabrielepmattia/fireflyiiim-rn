import { SET_APP_LOADED_PERSISTENT } from "src/actions/types";

export interface AppState {
  loadedPersistent: boolean
}

/*
 * Actions
 */

interface SetAppLoadedPersistentAction {
  type: typeof SET_APP_LOADED_PERSISTENT
  payload: boolean
}

export type AppActionType = SetAppLoadedPersistentAction