import { AppActionType } from './app/types';
import { AuthActionType } from './auth/types';
import { ConfigActionType } from './config/types';
import { CacheActionType } from './cache/types';

export type StoreActionType = AppActionType | AuthActionType | ConfigActionType | CacheActionType;
