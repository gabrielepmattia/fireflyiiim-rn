import { SET_AUTH_TOKEN, SET_AUTH_SIGNED } from "src/actions/types";

export interface AuthState {
  signed_in: boolean
  token: string
}

/*
 * Actions
 */

interface SetAuthTokenAction {
  type: typeof SET_AUTH_TOKEN
  payload: string
}

interface SetAuthSignedAction {
  type: typeof SET_AUTH_SIGNED
  payload: boolean
}

export type AuthActionType = SetAuthTokenAction | SetAuthSignedAction