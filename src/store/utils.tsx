import {
  setCacheAccounts,
  setCacheBudgets,
  setCacheCategories,
  setCacheDashboardSummary,
  setCacheDashboardTransactions,
  setCacheTags
} from "src/actions/cache"
import { ACCOUNT_TYPE_ALL } from "lib/fireflyiii-api/models/account"
import { FireflyIIIApi } from "lib/fireflyiii-api/api"
import ApiService from "../services/api.service";
import { store } from "./store"

const MODULE = "StoreUtils"

/*
 * Refreshers
 */

export const refreshAccounts = (): void => {
  console.debug(`[${MODULE}] refreshAccounts: started`)
  ApiService.getInstance().handler().getAccounts(ACCOUNT_TYPE_ALL)
    .then(accounts => {
      store.dispatch(setCacheAccounts(accounts))
      console.debug(`[${MODULE}] refreshAccounts: api ok`)
    })
    .catch(err => console.error(`[${MODULE}] refreshAccounts: api error: ${JSON.stringify(err)}`))
}

export const refreshDashboardSummary = (start: string, end: string): void => {
  console.debug(`[${MODULE}] refreshDashboardSummary: started`)
  ApiService.getInstance().handler().getSummary(start, end)
    .then(summary => {
      store.dispatch(setCacheDashboardSummary(summary))
      console.debug(`[${MODULE}] refreshDashboardSummary: api ok`)
    })
    .catch(err => console.error(`[${MODULE}] refreshDashboardSummary: api error: ${JSON.stringify(err)}`))
}

export const refreshDashboardTransactions = (type: string, page: string, start: string, end: string): void => {
  console.debug(`[${MODULE}] refreshDashboardTransactions: started`)
  ApiService.getInstance().handler().getTransactions(type, page, start, end)
    .then(transactions => {
      store.dispatch(setCacheDashboardTransactions(transactions))
      console.debug(`[${MODULE}] refreshDashboardTransactions: api ok`)
    })
    .catch(err => console.error(`[${MODULE}] refreshDashboardTransactions: api error: ${JSON.stringify(err)}`))
}

export const refreshCategories = (): void => {
  console.debug(`[${MODULE}] refreshCategories: started`)
  ApiService.getInstance().handler().getCategories()
    .then(categories => {
      store.dispatch(setCacheCategories(categories))
      console.debug(`[${MODULE}] refreshCategories: api ok`)
    })
    .catch(err => console.error(`[${MODULE}] refreshCategories: api error: ${JSON.stringify(err)}`))
}

export const refreshTags = (): void => {
  console.debug(`[${MODULE}] refreshTags: started`)
  ApiService.getInstance().handler().getTags()
    .then(tags => {
      store.dispatch(setCacheTags(tags))
      console.debug(`[${MODULE}] refreshTags: api ok`)
    })
    .catch(err => console.error(`[${MODULE}] refreshTags: api error: ${JSON.stringify(err)}`))
}

export const refreshBudgets = (): void => {
  console.debug(`[${MODULE}] refreshBudgets: started`)
  ApiService.getInstance().handler().getBudgets()
    .then(budgets => {
      store.dispatch(setCacheBudgets(budgets))
      console.debug(`[${MODULE}] refreshBudgets: api ok`)
    })
    .catch(err => console.error(`[${MODULE}] refreshBudgets: api error: ${JSON.stringify(err)}`))
}

