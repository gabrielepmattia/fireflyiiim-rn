import { createStore, combineReducers } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage'

// reducers
import authReducer from '../reducers/authReducer';
import appReducer from '../reducers/appReducer';
import configReducer from '../reducers/configReducer';
import cacheReducer from 'src/reducers/cacheReducer';

const rootReducer = combineReducers({
  app: appReducer,
  config: configReducer,
  auth: authReducer,
  cache: cacheReducer,
});

//export const configureStore = () => {
//  return createStore(rootReducer);
// };

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = createStore(persistedReducer)
export const persistor = persistStore(store)

/*
 * Types
 */

export type RootState = ReturnType<typeof rootReducer>

