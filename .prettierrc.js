module.exports = {
  bracketSpacing: true,
  jsxBracketSameLine: true,
  singleQuote: true,
  trailingComma: 'all',
  jsxSingleQuote: false,
  printWidth: 130,
};
